#ifndef LAB2_FPTHREAD_H
#define LAB2_FPTHREAD_H

#include <uTTCOSg2017/uTTCOSg.h>
#include <GPIO2017/ADSP_GPIO_interface.h>

#include "Lab2_FPThread.h"   // Should include links to other project header files

#include "GeneralCode_Lab0n1.h"
//To be commented away
#include <stdio.h>

#define FP_LED_8 0x0100 >>1
#define FP_LED_7 0x0080 >>1
#define FP_LED_2 0x0002
#define FP_LED_1 0x0001
#define FP_LED_3_4_5_6 0x3c

void My_Init_All(void);
void PWM_FP_LED8(void);
void PWM_FP_LED7(void);
void CLK_COUNTER_LED1_2(void);
void LAB0_Thread(void);
void FPSwitchOne_Thread(void);
void ReadFPSwitch_Thread(void);

extern volatile char ID_PWM_REB_LED_All;

#endif
