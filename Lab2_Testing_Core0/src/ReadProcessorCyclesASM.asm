.section L1_data
// .var
// Private data values

.section program;
.global _ReadProcessorCyclesASM;
.global _ResetProcessorCyclesToZeroASM;

#define returnValue_R0 R0

_ReadProcessorCyclesASM:
	LINK 20;
	returnValue_R0 = CYCLES;
	UNLINK;
	
_ReadProcessorCyclesASM.END:
	RTS;


_ResetProcessorCyclesToZeroASM:
	LINK 20;
	R0 = 0x0;
	CYCLES = R0;
	UNLINK;
_ResetProcessorCyclesToZeroASM.END:
	RTS;