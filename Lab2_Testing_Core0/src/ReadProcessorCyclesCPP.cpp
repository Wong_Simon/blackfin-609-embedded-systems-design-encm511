#include <sys/platform.h>
#include "adi_initialize.h"
#include "Lab2_FPThread.h"

#include <blackfin.h>

// Prototypes
unsigned long int ReadProcessorCyclesCPP(void);
void ResetProcessorCyclestoZeroCPP(void);
extern "C" unsigned long int ReadProcessorCyclesASM(void);
extern "C" void ResetProcessorCyclesToZeroASM(void);

unsigned long int ReadProcessorCyclesCPP(){
	//printf("Stub for ReadProcessorCyclesCPP\n");

	unsigned long int cyclesValue = ReadProcessorCyclesASM(); // The following does not work: cyclesValue = *pCYCLES

	return cyclesValue;
}

void ResetProcessorCyclestoZeroCPP(){
	ResetProcessorCyclesToZeroASM();
}
