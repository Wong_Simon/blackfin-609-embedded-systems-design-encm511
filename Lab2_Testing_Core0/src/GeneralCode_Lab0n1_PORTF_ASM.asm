#include "blackfin.h"

#define OUTPUT_PORTF_12TO15 0xF000
#define INPUT_PORTF_8TO11 0x0F00

#define PortPointer_P0 P0
#define PortPointer_P0.L P0.L
#define PortPointer_P0.H P0.H
#define ReturnPortPattern_R0 R0
#define ReturnPortPattern_R0.L R0.L
#define ReturnPortPattern_R0.H R0.H
#define tempMask_R1 R1
#define tempMask_R2 R2
#define InputParameter_DataToWrite_16bits_R0 R0

.section program;

.global _My_Read_GPIO_REB_Input_ASM; //extern "C" unsigned short int My_Read_GPIO_REB_Input(void) {
.global _My_Write_GPIO_REB_Output_ASM; //extern "C" void My_Write_GPIO_REB_Output (unsigned short int DataToWrite_16bits) {
.global _My_Init_GPIO_REB_Input_ASM; //extern "C" void My_Init_GPIO_REB_Input(void) {
.global _My_Init_GPIO_REB_Output_ASM; //extern "C" void My_Init_GPIO_REB_Output (void) {

_My_Read_GPIO_REB_Input_ASM:
	LINK 20;
	//extern "C" unsigned short int My_Read_GPIO_REB_Input(void) {
	//	unsigned short int PortPattern = *pREG_PORTF_DATA;
	PortPointer_P0.H = hi(REG_PORTF_DATA);	
	PortPointer_P0.L = lo(REG_PORTF_DATA);	
	//	*pREG_PORTF_DATA = PortPattern & INPUT_PORTF_8TO11;
	tempMask_R1 = INPUT_PORTF_8TO11 (Z);
	ReturnPortPattern_R0 = W[PortPointer_P0](Z);
	ReturnPortPattern_R0 = ReturnPortPattern_R0 & tempMask_R1;
	W[PortPointer_P0]= ReturnPortPattern_R0.L;
	//}
	ssync;
	UNLINK;
_My_Read_GPIO_REB_Input_ASM.END:
RTS;

_My_Write_GPIO_REB_Output_ASM:
	LINK 20;
	//extern "C" void My_Write_GPIO_REB_Output (unsigned short int DataToWrite_16bits) {
	//	unsigned short int PortPattern = *pREG_PORTF_DATA;
	PortPointer_P0.H = hi(REG_PORTF_DATA);	
	PortPointer_P0.L = lo(REG_PORTF_DATA);	

	//	*pREG_PORTF_DATA = (DataToWrite_16bits & OUTPUT_PORTF_12TO15) | (PortPattern & ~OUTPUT_PORTF_12TO15);
	tempMask_R1 = OUTPUT_PORTF_12TO15(Z);
	tempMask_R1 = InputParameter_DataToWrite_16bits_R0 & tempMask_R1;
	
	tempMask_R2 = OUTPUT_PORTF_12TO15(Z);
	tempMask_R2 = ~tempMask_R2;
	
	ReturnPortPattern_R0 = W[PortPointer_P0](Z);
	tempMask_R2 = tempMask_R2 & ReturnPortPattern_R0;
	
	ReturnPortPattern_R0 = tempMask_R1 | tempMask_R2;
	W[PortPointer_P0] = ReturnPortPattern_R0.L;
	//}
	ssync;
	UNLINK;
_My_Write_GPIO_REB_Output_ASM.END:
RTS;

_My_Init_GPIO_REB_Input_ASM:
	LINK 20;
	//extern "C" void My_Init_GPIO_REB_Input(void) {
	
	//	unsigned short int PortPattern = *pREG_PORTF_INEN;
	PortPointer_P0.H = hi(REG_PORTF_INEN);	
	PortPointer_P0.L = lo(REG_PORTF_INEN);
	ReturnPortPattern_R0 = W[PortPointer_P0](Z);
	//	PortPattern = PortPattern | INPUT_PORTF_8TO11;
	tempMask_R1 = INPUT_PORTF_8TO11;
	ReturnPortPattern_R0 = ReturnPortPattern_R0 | tempMask_R1;
	W[PortPointer_P0] = ReturnPortPattern_R0;
	
	//	PortPattern = *pREG_PORTF_POL;
	PortPointer_P0.H = hi(REG_PORTF_POL);	
	PortPointer_P0.L = lo(REG_PORTF_POL);
	ReturnPortPattern_R0 = W[PortPointer_P0](Z);
	//	PortPattern = PortPattern & ~INPUT_PORTF_8TO11;
	tempMask_R1 = ~INPUT_PORTF_8TO11;
	ReturnPortPattern_R0 = ReturnPortPattern_R0 & tempMask_R1;
	W[PortPointer_P0] = ReturnPortPattern_R0;
	
	//	PortPattern = *pREG_PORTF_DIR;
	PortPointer_P0.H = hi(REG_PORTF_DIR);	
	PortPointer_P0.L = lo(REG_PORTF_DIR);
	ReturnPortPattern_R0 = W[PortPointer_P0](Z);
	//	PortPattern = PortPattern & ~INPUT_PORTF_8TO11;
	tempMask_R1 = ~INPUT_PORTF_8TO11;
	ReturnPortPattern_R0 = ReturnPortPattern_R0 & tempMask_R1;
	W[PortPointer_P0] = ReturnPortPattern_R0;
	//}
	ssync;
	UNLINK;
_My_Init_GPIO_REB_Input_ASM.END:
RTS;

_My_Init_GPIO_REB_Output_ASM:
	LINK 20;
	//extern "C" void My_Init_GPIO_REB_Output (void) {
	
	//	unsigned short int PortPattern = *pREG_PORTF_INEN;
	PortPointer_P0.H = hi(REG_PORTF_INEN);	
	PortPointer_P0.L = lo(REG_PORTF_INEN);
	ReturnPortPattern_R0 = W[PortPointer_P0](Z);
	//	PortPattern = PortPattern & ~OUTPUT_PORTF_12TO15;
	tempMask_R1 = OUTPUT_PORTF_12TO15(Z);
	tempMask_R1 = ~tempMask_R1;
	ReturnPortPattern_R0 = ReturnPortPattern_R0 & tempMask_R1;
	W[PortPointer_P0] = ReturnPortPattern_R0;
	
	//	PortPattern = *pREG_PORTF_POL;
	PortPointer_P0.H = hi(REG_PORTF_POL);	
	PortPointer_P0.L = lo(REG_PORTF_POL);
	ReturnPortPattern_R0 = W[PortPointer_P0](Z);
	//	PortPattern = PortPattern & ~OUTPUT_PORTF_12TO15;
	tempMask_R1 = OUTPUT_PORTF_12TO15(Z);
	tempMask_R1 = ~tempMask_R1;
	ReturnPortPattern_R0 = ReturnPortPattern_R0 & tempMask_R1;
	W[PortPointer_P0] = ReturnPortPattern_R0;
	
	//	PortPattern = *pREG_PORTF_DIR;
	PortPointer_P0.H = hi(REG_PORTF_DIR);	
	PortPointer_P0.L = lo(REG_PORTF_DIR);
	ReturnPortPattern_R0 = W[PortPointer_P0](Z);
	//	PortPattern = PortPattern | OUTPUT_PORTF_12TO15;
	tempMask_R1 = OUTPUT_PORTF_12TO15(Z);
	ReturnPortPattern_R0 = ReturnPortPattern_R0 | tempMask_R1;
	W[PortPointer_P0] = ReturnPortPattern_R0;
	//}
	ssync;
	UNLINK;
_My_Init_GPIO_REB_Output_ASM.END:
RTS;
