/*****************************************************************************
 * 20190918_BF609_Lab0_Core0.cpp
 *****************************************************************************/

#include "20190918_BF609_Lab0_Core0.h"

#include <sys/platform.h>
#include <sys/adi_core.h>
#include <ccblkfn.h>
#include "adi_initialize.h"

/** 
 * If you want to use command program arguments, then place them in the following string. 
 */

char __argv_string[] = "";

int main(int argc, char *argv[])
{
	adi_initComponents();
	
	/**
	 * The default startup code does not include any functionality to allow
	 * core 0 to enable core 1. A convenient way to enable
	 * core 1 is to use the adi_core_enable function. 
	 */

//#define __ADSPBF609__ 0x42
#ifdef __ADSPBF533__
	printf("Start BF533 Lab 0\n");
	Start_Lab0();
#endif


#ifdef __ADSPBF609__
	adi_core_enable(ADI_CORE_1);
	printf("Start BF609 Lab 0\n");
	Start_Lab0();
#endif

	printf("Lab 0 Complete\n");
	return 0;

}

