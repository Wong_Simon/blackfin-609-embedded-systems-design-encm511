################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
ASM_SRCS += \
../src/ReadCycles_ASM.asm 

CPP_SRCS += \
../src/Coffeepot_BF609_CTimer_Core0_EUNIT2017_main.cpp \
../src/CoreTimer.cpp \
../src/CoreTimer_Functions.cpp \
../src/ISR_GeneralCode.cpp 

SRC_OBJS += \
./src/Coffeepot_BF609_CTimer_Core0_EUNIT2017_main.doj \
./src/CoreTimer.doj \
./src/CoreTimer_Functions.doj \
./src/ISR_GeneralCode.doj \
./src/ReadCycles_ASM.doj 

ASM_DEPS += \
./src/ReadCycles_ASM.d 

CPP_DEPS += \
./src/Coffeepot_BF609_CTimer_Core0_EUNIT2017_main.d \
./src/CoreTimer.d \
./src/CoreTimer_Functions.d \
./src/ISR_GeneralCode.d 


# Each subdirectory must supply rules for building sources it contributes
src/Coffeepot_BF609_CTimer_Core0_EUNIT2017_main.doj: ../src/Coffeepot_BF609_CTimer_Core0_EUNIT2017_main.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn -c -file-attr ProjectName="Coffeepot_BF609_CTimer_Core0" -proc ADSP-BF609 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG -DADI_MCAPI @includes-82c693ecd80d41cb0564981c2c614e94.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/Coffeepot_BF609_CTimer_Core0_EUNIT2017_main.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/CoreTimer.doj: ../src/CoreTimer.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn -c -file-attr ProjectName="Coffeepot_BF609_CTimer_Core0" -proc ADSP-BF609 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG -DADI_MCAPI @includes-82c693ecd80d41cb0564981c2c614e94.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/CoreTimer.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/CoreTimer_Functions.doj: ../src/CoreTimer_Functions.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn -c -file-attr ProjectName="Coffeepot_BF609_CTimer_Core0" -proc ADSP-BF609 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG -DADI_MCAPI @includes-82c693ecd80d41cb0564981c2c614e94.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/CoreTimer_Functions.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/ISR_GeneralCode.doj: ../src/ISR_GeneralCode.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn -c -file-attr ProjectName="Coffeepot_BF609_CTimer_Core0" -proc ADSP-BF609 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG -DADI_MCAPI @includes-82c693ecd80d41cb0564981c2c614e94.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/ISR_GeneralCode.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/ReadCycles_ASM.doj: ../src/ReadCycles_ASM.asm
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin Assembler'
	easmblkfn -file-attr ProjectName="Coffeepot_BF609_CTimer_Core0" -proc ADSP-BF609 -si-revision any -g -DCORE0 -D_DEBUG -DADI_MCAPI @includes-39d7257bfbffa9e81305bbce39275ad6.txt -gnu-style-dependencies -MM -Mo "src/ReadCycles_ASM.d" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


