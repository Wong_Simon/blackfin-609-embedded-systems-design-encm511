/*
 * ISR_GeneralCode.cpp
 *
 *  Created on: Nov 25, 2019
 *      Author: Simon
 */
#include "../../Coffeepot_Sim533_CTimer/src/TDD_Core_Timer.h"

#pragma interrupt
void CoreTimer_ISR (void){
	ISR_Count_test++;
}
