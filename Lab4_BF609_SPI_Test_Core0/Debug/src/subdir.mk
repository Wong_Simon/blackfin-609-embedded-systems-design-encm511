################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
ASM_SRCS += \
../src/SPI_GeneralFunctions_ASM.asm 

CPP_SRCS += \
../src/LCD_SPI_GeneralFunctions.cpp \
../src/LCD_SPI_Testing.cpp \
../src/Lab4_BF609_SPI_Test_Core0_EUNIT2017_main.cpp \
../src/SetBoard_LED_SPI_SoftConfig_BF609.cpp \
../src/Set_Pinmux_config.cpp 

SRC_OBJS += \
./src/LCD_SPI_GeneralFunctions.doj \
./src/LCD_SPI_Testing.doj \
./src/Lab4_BF609_SPI_Test_Core0_EUNIT2017_main.doj \
./src/SPI_GeneralFunctions_ASM.doj \
./src/SetBoard_LED_SPI_SoftConfig_BF609.doj \
./src/Set_Pinmux_config.doj 

ASM_DEPS += \
./src/SPI_GeneralFunctions_ASM.d 

CPP_DEPS += \
./src/LCD_SPI_GeneralFunctions.d \
./src/LCD_SPI_Testing.d \
./src/Lab4_BF609_SPI_Test_Core0_EUNIT2017_main.d \
./src/SetBoard_LED_SPI_SoftConfig_BF609.d \
./src/Set_Pinmux_config.d 


# Each subdirectory must supply rules for building sources it contributes
src/LCD_SPI_GeneralFunctions.doj: ../src/LCD_SPI_GeneralFunctions.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn -c -file-attr ProjectName="Lab4_BF609_SPI_Test_Core0" -proc ADSP-BF609 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG -DADI_MCAPI @includes-344a4b8b268a9d1a0584dbfc6f363cec.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/LCD_SPI_GeneralFunctions.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/LCD_SPI_Testing.doj: ../src/LCD_SPI_Testing.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn -c -file-attr ProjectName="Lab4_BF609_SPI_Test_Core0" -proc ADSP-BF609 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG -DADI_MCAPI @includes-344a4b8b268a9d1a0584dbfc6f363cec.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/LCD_SPI_Testing.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/Lab4_BF609_SPI_Test_Core0_EUNIT2017_main.doj: ../src/Lab4_BF609_SPI_Test_Core0_EUNIT2017_main.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn -c -file-attr ProjectName="Lab4_BF609_SPI_Test_Core0" -proc ADSP-BF609 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG -DADI_MCAPI @includes-344a4b8b268a9d1a0584dbfc6f363cec.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/Lab4_BF609_SPI_Test_Core0_EUNIT2017_main.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/SPI_GeneralFunctions_ASM.doj: ../src/SPI_GeneralFunctions_ASM.asm
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin Assembler'
	easmblkfn -file-attr ProjectName="Lab4_BF609_SPI_Test_Core0" -proc ADSP-BF609 -si-revision any -g -DCORE0 -D_DEBUG -DADI_MCAPI @includes-055e72d7ef37168bb8648fb40c477dc3.txt -gnu-style-dependencies -MM -Mo "src/SPI_GeneralFunctions_ASM.d" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/SetBoard_LED_SPI_SoftConfig_BF609.doj: ../src/SetBoard_LED_SPI_SoftConfig_BF609.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn -c -file-attr ProjectName="Lab4_BF609_SPI_Test_Core0" -proc ADSP-BF609 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG -DADI_MCAPI @includes-344a4b8b268a9d1a0584dbfc6f363cec.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/SetBoard_LED_SPI_SoftConfig_BF609.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/Set_Pinmux_config.doj: ../src/Set_Pinmux_config.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn -c -file-attr ProjectName="Lab4_BF609_SPI_Test_Core0" -proc ADSP-BF609 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG -DADI_MCAPI @includes-344a4b8b268a9d1a0584dbfc6f363cec.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/Set_Pinmux_config.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


