/*
 * LCD_SPI_GeneralFunctions.cpp
 *
 *  Created on: Nov 27, 2019
 *      Author: Simon
 */
#include "LCD_SPI_GeneralFunctions.h"
extern volatile unsigned short int Screen_Init_Array [END_OF_SCREEN_INIT_ARRAY] = {0x0030, 0x0030, 0x000F, 0x0001}; //remember that bit 10 need to be on from this point forward
extern volatile unsigned char FirstMessageArray[END_OF_FIRSTMESAGE] = "Testing  ";


void SPI_Write_ebitPulse (unsigned short int WriteVal, bool CommandMode) {
	unsigned short int Command = 0x0;
	if (CommandMode) Command = RS_BIT;
	while(REB_SPI_Ready() == false) {} // Wait
	REB_Write_SPI(WriteVal | E_BIT | Command);
	ssync();

	while(REB_SPI_Ready() == false) {} // Wait
	REB_Write_SPI(WriteVal | Command);
	ssync();

}

void SPI_InitScreenArray (void) {
	for (int i =0; i<END_OF_SCREEN_INIT_ARRAY; i++){
		SPI_Write_ebitPulse(Screen_Init_Array[i], false);
		Waitasec();
	}
}

void SPI_ClearScreen (void) {
		SPI_Write_ebitPulse(CLEAR_SCREEN_BIT, false);
		Waitasec();
}

void SPI_WriteMessageArray (unsigned char * StringWithNullChar, unsigned short int EndIndex_WriteMessage) {

	for (int i =0; i<EndIndex_WriteMessage; i++){

		SPI_Write_ebitPulse(StringWithNullChar[i], true);
		Waitasec();

	}
}

//todo remove
void Waitasec(void){
	int temp = TIC_DELAY;
	while (temp>0) {temp--;}
}

