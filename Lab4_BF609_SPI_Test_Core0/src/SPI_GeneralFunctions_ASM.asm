/*
 * SPI_GeneralFunctions_ASM.asm
 *
 *  Created on: Nov 27, 2019
 *      Author: Simon
 */
 #include <blackfin.h>
.section program;
.global _Write_TFIFO_RFIFO;
_Write_TFIFO_RFIFO:
	LINK 20;
	nop;
	nop;
	nop;
	//P0.L = lo(SPI0_TFIFO);
	//P0.H = hi(SPI0_TFIFO);
	[P0] = R0;
	UNLINK;
_Write_TFIFO_RFIFO.END:
	RTS;