/*****************************************************************************
 * 2019_Sim533_PostLab1Practice.cpp
 *****************************************************************************/

#include <sys/platform.h>
#include "adi_initialize.h"
#include "2019_Sim533_PostLab1Practice.h"
#include <stdio.h>
/** 
 * If you want to use command program arguments, then place them in the following string. 
 */
char __argv_string[] = "";

int main(int argc, char *argv[])
{
	/**
	 * Initialize managed drivers and/or services that have been added to 
	 * the project.
	 * @return zero on success 
	 */
	adi_initComponents();
	
	/* Begin adding your custom code here */

	unsigned long int temp = 0xF0002000;
	unsigned short int Q2BValue = temp;
	if (Q2BValue < 0x4000){
		printf("1\n");
	}

	return 0;
}

