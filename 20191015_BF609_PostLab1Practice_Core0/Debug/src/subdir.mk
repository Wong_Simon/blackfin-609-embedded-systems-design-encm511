################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
ASM_SRCS += \
../src/PostLab1_Practice_GeneralCodeASM.asm 

CPP_SRCS += \
../src/20191015_BF609_PostLab1Practice_Core0.cpp \
../src/PostLab1_Practice_GeneralCode.cpp 

SRC_OBJS += \
./src/20191015_BF609_PostLab1Practice_Core0.doj \
./src/PostLab1_Practice_GeneralCode.doj \
./src/PostLab1_Practice_GeneralCodeASM.doj 

ASM_DEPS += \
./src/PostLab1_Practice_GeneralCodeASM.d 

CPP_DEPS += \
./src/20191015_BF609_PostLab1Practice_Core0.d \
./src/PostLab1_Practice_GeneralCode.d 


# Each subdirectory must supply rules for building sources it contributes
src/20191015_BF609_PostLab1Practice_Core0.doj: ../src/20191015_BF609_PostLab1Practice_Core0.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn -c -file-attr ProjectName="20191015_BF609_PostLab1Practice_Core0" -proc ADSP-BF609 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG -DADI_MCAPI @includes-ef22e5430212f95756f1d2a6106170a5.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/20191015_BF609_PostLab1Practice_Core0.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/PostLab1_Practice_GeneralCode.doj: ../src/PostLab1_Practice_GeneralCode.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn -c -file-attr ProjectName="20191015_BF609_PostLab1Practice_Core0" -proc ADSP-BF609 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG -DADI_MCAPI @includes-ef22e5430212f95756f1d2a6106170a5.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/PostLab1_Practice_GeneralCode.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/PostLab1_Practice_GeneralCodeASM.doj: ../src/PostLab1_Practice_GeneralCodeASM.asm
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin Assembler'
	easmblkfn -file-attr ProjectName="20191015_BF609_PostLab1Practice_Core0" -proc ADSP-BF609 -si-revision any -g -DCORE0 -D_DEBUG -DADI_MCAPI @includes-60b7d63d8034156d14bfa185900749cf.txt -gnu-style-dependencies -MM -Mo "src/PostLab1_Practice_GeneralCodeASM.d" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


