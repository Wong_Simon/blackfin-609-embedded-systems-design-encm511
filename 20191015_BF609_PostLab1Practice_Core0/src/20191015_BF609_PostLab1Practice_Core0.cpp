/*****************************************************************************
 * 20191015_BF609_PostLab1Practice_Core0.cpp
 *****************************************************************************/

#include <sys/platform.h>
#include <sys/adi_core.h>
#include <ccblkfn.h>
#include "adi_initialize.h"
#include "20191015_BF609_PostLab1Practice_Core0.h"

/** 
 * If you want to use command program arguments, then place them in the following string. 
 */
char __argv_string[] = "";

int main(int argc, char *argv[])
{
	adi_initComponents();

#ifdef __ADSPBF533__
	printf("Start BF533 Lab 0\n");
	Start_Practice_PostLab1();
#endif

#ifdef __ADSPBF609__
	adi_core_enable(ADI_CORE_1);
	printf("Start BF609 Lab 0\n");
	Start_Practice_PostLab1();
#endif

	return 0;
}

