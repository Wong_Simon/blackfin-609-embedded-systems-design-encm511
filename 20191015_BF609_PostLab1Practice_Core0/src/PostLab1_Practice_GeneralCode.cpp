#include "20191015_BF609_PostLab1Practice_Core0.h"

//Prototypes
extern "C" unsigned long int Get_CycleASM(void);

#define SECOND_ONE 480000000

void Start_Practice_PostLab1(){
	printf("inside start\n");
	unsigned short int array[16];
	for (int i = 0; i<16; i++){
		array[i] = i*8;
		printf("%i\n", array[i]);
	}
	printf("%i\n", Get_CycleASM);

#if 0
	unsigned long int CycleCurrent = GetCycleASM();
	unsigned long int CyclePeriod = SECOND_ONE/2;
	unsigned long int CycleEnd = CycleCurrent + CyclePeriod;

	while (1) {
		//Wait until end of the cycle counter
		while (CycleCurrent < CycleEnd) {
			CycleCurrent = GetCycleASM();
		}
		CycleEnd = CycleCurrent + CyclePeriod;
		printf("CycleCurrent: %i\n", CycleCurrent);
		printf("CycleEnd:     %i\n", CycleEnd);
	}
#endif
}
