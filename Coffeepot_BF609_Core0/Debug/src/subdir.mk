################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
ASM_SRCS += \
../src/CoffeePotASM.asm \
../src/GeneralCode_Lab0n1_PORTF_ASM.asm \
../src/ReadProcessorCyclesASM.asm 

CPP_SRCS += \
../src/Coffeepot_BF609_Core0.cpp \
../src/Coffeepot_GeneralCode.cpp \
../src/CoreTimer_Functions.cpp \
../src/GeneralCode_Lab0n1.cpp \
../src/ISR_GeneralCode.cpp \
../src/ReadProcessorCyclesCPP.cpp 

SRC_OBJS += \
./src/CoffeePotASM.doj \
./src/Coffeepot_BF609_Core0.doj \
./src/Coffeepot_GeneralCode.doj \
./src/CoreTimer_Functions.doj \
./src/GeneralCode_Lab0n1.doj \
./src/GeneralCode_Lab0n1_PORTF_ASM.doj \
./src/ISR_GeneralCode.doj \
./src/ReadProcessorCyclesASM.doj \
./src/ReadProcessorCyclesCPP.doj 

ASM_DEPS += \
./src/CoffeePotASM.d \
./src/GeneralCode_Lab0n1_PORTF_ASM.d \
./src/ReadProcessorCyclesASM.d 

CPP_DEPS += \
./src/Coffeepot_BF609_Core0.d \
./src/Coffeepot_GeneralCode.d \
./src/CoreTimer_Functions.d \
./src/GeneralCode_Lab0n1.d \
./src/ISR_GeneralCode.d \
./src/ReadProcessorCyclesCPP.d 


# Each subdirectory must supply rules for building sources it contributes
src/CoffeePotASM.doj: ../src/CoffeePotASM.asm
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin Assembler'
	easmblkfn -file-attr ProjectName="Coffeepot_BF609_Core0" -proc ADSP-BF609 -si-revision any -g -DCORE0 -D_DEBUG -DADI_MCAPI @includes-3dbe251763f78cf50c412d192e2719e8.txt -gnu-style-dependencies -MM -Mo "src/CoffeePotASM.d" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/Coffeepot_BF609_Core0.doj: ../src/Coffeepot_BF609_Core0.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn -c -file-attr ProjectName="Coffeepot_BF609_Core0" -proc ADSP-BF609 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG -DADI_MCAPI @includes-f742f2628bcf46ec9f5c43216ea6faea.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/Coffeepot_BF609_Core0.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/Coffeepot_GeneralCode.doj: ../src/Coffeepot_GeneralCode.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn -c -file-attr ProjectName="Coffeepot_BF609_Core0" -proc ADSP-BF609 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG -DADI_MCAPI @includes-f742f2628bcf46ec9f5c43216ea6faea.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/Coffeepot_GeneralCode.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/CoreTimer_Functions.doj: ../src/CoreTimer_Functions.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn -c -file-attr ProjectName="Coffeepot_BF609_Core0" -proc ADSP-BF609 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG -DADI_MCAPI @includes-f742f2628bcf46ec9f5c43216ea6faea.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/CoreTimer_Functions.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/GeneralCode_Lab0n1.doj: ../src/GeneralCode_Lab0n1.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn -c -file-attr ProjectName="Coffeepot_BF609_Core0" -proc ADSP-BF609 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG -DADI_MCAPI @includes-f742f2628bcf46ec9f5c43216ea6faea.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/GeneralCode_Lab0n1.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/GeneralCode_Lab0n1_PORTF_ASM.doj: ../src/GeneralCode_Lab0n1_PORTF_ASM.asm
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin Assembler'
	easmblkfn -file-attr ProjectName="Coffeepot_BF609_Core0" -proc ADSP-BF609 -si-revision any -g -DCORE0 -D_DEBUG -DADI_MCAPI @includes-3dbe251763f78cf50c412d192e2719e8.txt -gnu-style-dependencies -MM -Mo "src/GeneralCode_Lab0n1_PORTF_ASM.d" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/ISR_GeneralCode.doj: ../src/ISR_GeneralCode.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn -c -file-attr ProjectName="Coffeepot_BF609_Core0" -proc ADSP-BF609 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG -DADI_MCAPI @includes-f742f2628bcf46ec9f5c43216ea6faea.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/ISR_GeneralCode.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/ReadProcessorCyclesASM.doj: ../src/ReadProcessorCyclesASM.asm
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin Assembler'
	easmblkfn -file-attr ProjectName="Coffeepot_BF609_Core0" -proc ADSP-BF609 -si-revision any -g -DCORE0 -D_DEBUG -DADI_MCAPI @includes-3dbe251763f78cf50c412d192e2719e8.txt -gnu-style-dependencies -MM -Mo "src/ReadProcessorCyclesASM.d" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/ReadProcessorCyclesCPP.doj: ../src/ReadProcessorCyclesCPP.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn -c -file-attr ProjectName="Coffeepot_BF609_Core0" -proc ADSP-BF609 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG -DADI_MCAPI @includes-f742f2628bcf46ec9f5c43216ea6faea.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/ReadProcessorCyclesCPP.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


