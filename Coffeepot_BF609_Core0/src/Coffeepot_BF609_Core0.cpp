/*****************************************************************************
 * Coffepot_BF609_Core0.cpp
 *****************************************************************************/

#include "Coffeepot_BF609_Core0.h"

#include <sys/platform.h>
#include <sys/adi_core.h>
#include <ccblkfn.h>
#include "adi_initialize.h"

char __argv_string[] = "";

int main(int argc, char *argv[])
{
	adi_initComponents();
	
	adi_core_enable(ADI_CORE_1);

	My_CoffeePot_Main();

	return 0;
}

