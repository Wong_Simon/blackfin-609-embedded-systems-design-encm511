/*
 * CoreTimer_Functions.h
 *
 *  Created on: Nov 25, 2019
 *      Author: Simon
 */

#ifndef CORETIMER_FUNCTIONS_H_
#define CORETIMER_FUNCTIONS_H_

#define TINT_BIT 0x8

#define TIC 2400000
#define ONE_SECOND 100*TIC
#define PERIOD ONE_SECOND
#define INIT_COUNT ONE_SECOND

#define TINT_BIT 0x8
#define AUTORELOAD_BIT 0x4
#define TMPWR_BIT 0x1
#define TMREN_BIT 0x2
#define FIRST4BITS 0xF

#define IMASK_IVTMR 0x40

#include "../../ENCM511_SpecificFiles/ENCM511_include/CoreTimer_Library.h"
#include <stdio.h>
#include <blackfin.h>
#include <sys/exception.h>

extern "C" unsigned long long int ReadCycles_ASM(void);

void My_Init_CoreTimer(unsigned long int tperiod, unsigned long int tcount);
void My_Start_CoreTimer(void);
void My_Stop_CoreTimer(void);
unsigned long int My_Read_CoreTimer(void);
bool My_Done_CoreTimer(void);
void My_TimedWaitOnCoreTimer(void);

bool My_CoreTimer_My_Main(void);

void My_Disable_CoreTimerInterrupts(void);
void My_Enable_CoreTimerInterrupts(void);
void My_Toggle_CoreTimerInterrupts(void);
void My_SetUpCoreTimerInterrupts(void);
void My_Register_CoreTimerISR(void);


//Read the registers
enum CORETIMER_REG {DONT_KNOW, REG_COUNTER, REG_PERIOD, REG_TSCALE, REG_CNTRL};
unsigned long int My_PlannedCoreTimerRegisterValue(CORETIMER_REG regName);
unsigned long int My_ReadCoreTimerRegister(CORETIMER_REG regName);
void My_WriteCoreTimerRegister(CORETIMER_REG regName, unsigned long int newValue);

//ISR stuff
void CoreTimer_ISR (void);
extern volatile bool ISR_Allow_FastForward;

//Debug stuff
#define SHOW_DEBUG_CONSOLE_INFORMATION false

inline void Show_Debug_Console_Information(char *DebugInfo){
	if (SHOW_DEBUG_CONSOLE_INFORMATION){
		printf("%s", DebugInfo);
	}
}




#endif /* CORETIMER_FUNCTIONS_H_ */
