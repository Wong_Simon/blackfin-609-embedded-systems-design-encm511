/*
 * ISR_GeneralCode.cpp
 *
 *  Created on: Nov 25, 2019
 *      Author: Simon
 */
#include "CoreTimer_Functions.h"

#pragma interrupt
void CoreTimer_ISR (void){
	ISR_Allow_FastForward = true;
}
