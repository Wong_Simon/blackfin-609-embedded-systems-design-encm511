/*****************************************************************************
 * Coffepot_BF609_Core0.h
 *****************************************************************************/

#ifndef __COFFEPOT_BF609_CORE0_H__
#define __COFFEPOT_BF609_CORE0_H__

/* Add your custom header content here */
#include <stdio.h>
#include "../../ENCM511_SpecificFiles/ENCM511_include/CoffeePot_SimulatorFunctions2017.h"
#include "../../ENCM511_SpecificFiles/ENCM511_include/CoffeePot_SimulatorStructures2017.h"
#include "../../ENCM511_SpecificFiles/ENCM511_include/CoreTimer_Library.h"
#include "GeneralCode_Lab0n1.h"
#include "CoreTimer_Functions.h"

void My_CoffeePot_Main(void);

#define COFFEEPOT_INSERTED_FIXED 1 << 11
#define COFFEEPOT_LED_BITS_MASK 0xF000
#define BITS_4_TO_15 0xFFF0

#define HEATBOOSTREG 15
#define HEATER_REG_MAX 200
#define HEATER_REG_IDLE HEATER_REG_MAX/10
#define HEATER_REG_DECAY 15
#define BOILING_TEMP 80
#define IDEAL_TEMP 95
#define MAX_TEMP 100

#define EVAPORATION_RATE 30
#define EVAPORATION_OFFSET EVAPORATION_RATE/2
#define HALF_WATER_REG_DECAY 10/2
#define WATER_INFLOW_MAX 80 //This fills the coffeepot to MAX_WATER_LEVEL
#define MAX_WATER_LEVEL 400

void My_Init_CoffeePots(unsigned short int NumberOfCoffeePots);
COFFEEPOT_DEVICE * My_Plug_TurnOn_InitAll_AddCoffee_CoffeePot(COFFEEPOT_ID COFFEEPOT, char coffeePotName[]);
void Overall_CoffeePot_Control_Test(COFFEEPOT_ID COFFEEPOT, char coffeePotName[]);

COFFEEPOT_DEVICE * My_Plug_TurnOn_CoffeePot(COFFEEPOT_ID COFFEEPOT, char coffeePotName[]);
void My_Init_LED_CoffeePot(COFFEEPOT_DEVICE * CoffeePot_X);
void Test_LED(COFFEEPOT_DEVICE * CoffeePot_X);
void My_WriteLED_CoffeePot(COFFEEPOT_DEVICE * CoffeePot_X, short int WriteLEDValue_Coffeepot);
void My_Init_Water_CoffeePot(COFFEEPOT_DEVICE * CoffeePot_X);
void My_Init_Heater_CoffeePot(COFFEEPOT_DEVICE * CoffeePot_X);
void Test_Water_Failure(COFFEEPOT_DEVICE * CoffeePot_X);
void Test_Heater_Failure(COFFEEPOT_DEVICE * CoffeePot_X);
void Fill_CoffeePot_MaxWater(COFFEEPOT_DEVICE * CoffeePot_X);
void My_Water_Control(COFFEEPOT_DEVICE * CoffeePot_X, bool CoffeeIsReady);
void My_Heater_Control(COFFEEPOT_DEVICE * CoffeePot_X, bool CoffeeIsReady);
void Add_Coffee(COFFEEPOT_DEVICE * CoffeePot_X);
extern "C" void InitLED_TestLED_ASM(COFFEEPOT_DEVICE * CoffeePot_X);

#endif /* __COFFEPOT_BF609_CORE0_H__ */
