################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
ASM_SRCS += \
../ENCM511_src/Idle_WaitForInterrupts_ASM.asm \
../ENCM511_src/ReadCycles_ASM.asm 

CPP_SRCS += \
../ENCM511_src/Example_faultyLED1_Thread.cpp \
../ENCM511_src/Example_uTTCOSg2017_main.cpp 

SRC_OBJS += \
./ENCM511_src/Example_faultyLED1_Thread.doj \
./ENCM511_src/Example_uTTCOSg2017_main.doj \
./ENCM511_src/Idle_WaitForInterrupts_ASM.doj \
./ENCM511_src/ReadCycles_ASM.doj 

ASM_DEPS += \
./ENCM511_src/Idle_WaitForInterrupts_ASM.d \
./ENCM511_src/ReadCycles_ASM.d 

CPP_DEPS += \
./ENCM511_src/Example_faultyLED1_Thread.d \
./ENCM511_src/Example_uTTCOSg2017_main.d 


# Each subdirectory must supply rules for building sources it contributes
ENCM511_src/Example_faultyLED1_Thread.doj: ../ENCM511_src/Example_faultyLED1_Thread.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn -c -file-attr ProjectName="ENCM511_SpecificFiles" -proc ADSP-BF609 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG -DADI_MCAPI @includes-88aa5d19b9f6db3ec6cd0c577018a78d.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "ENCM511_src/Example_faultyLED1_Thread.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

ENCM511_src/Example_uTTCOSg2017_main.doj: ../ENCM511_src/Example_uTTCOSg2017_main.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn -c -file-attr ProjectName="ENCM511_SpecificFiles" -proc ADSP-BF609 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG -DADI_MCAPI @includes-88aa5d19b9f6db3ec6cd0c577018a78d.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "ENCM511_src/Example_uTTCOSg2017_main.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

ENCM511_src/Idle_WaitForInterrupts_ASM.doj: ../ENCM511_src/Idle_WaitForInterrupts_ASM.asm
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin Assembler'
	easmblkfn -file-attr ProjectName="ENCM511_SpecificFiles" -proc ADSP-BF609 -si-revision any -g -DCORE0 -D_DEBUG -DADI_MCAPI @includes-9776bdbf06e8e26200756f6962dae64f.txt -gnu-style-dependencies -MM -Mo "ENCM511_src/Idle_WaitForInterrupts_ASM.d" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

ENCM511_src/ReadCycles_ASM.doj: ../ENCM511_src/ReadCycles_ASM.asm
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin Assembler'
	easmblkfn -file-attr ProjectName="ENCM511_SpecificFiles" -proc ADSP-BF609 -si-revision any -g -DCORE0 -D_DEBUG -DADI_MCAPI @includes-9776bdbf06e8e26200756f6962dae64f.txt -gnu-style-dependencies -MM -Mo "ENCM511_src/ReadCycles_ASM.d" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


