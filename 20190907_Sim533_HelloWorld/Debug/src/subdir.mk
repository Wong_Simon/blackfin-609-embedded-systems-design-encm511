################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
ASM_SRCS += \
../src/hello.asm 

CPP_SRCS += \
../src/Sim533_20190908_HelloWorld.cpp 

SRC_OBJS += \
./src/Sim533_20190908_HelloWorld.doj \
./src/hello.doj 

ASM_DEPS += \
./src/hello.d 

CPP_DEPS += \
./src/Sim533_20190908_HelloWorld.d 


# Each subdirectory must supply rules for building sources it contributes
src/Sim533_20190908_HelloWorld.doj: ../src/Sim533_20190908_HelloWorld.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn -c -file-attr ProjectName="20190907_Sim533_HelloWorld" -proc ADSP-BF533 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG @includes-c5fb4a14d8a1b5a3c10c743bb9bac1f1.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/Sim533_20190908_HelloWorld.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/hello.doj: ../src/hello.asm
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin Assembler'
	easmblkfn -file-attr ProjectName="20190907_Sim533_HelloWorld" -proc ADSP-BF533 -si-revision any -g -DCORE0 -D_DEBUG @includes-1885db94ab879ddb7f70c598c0b764a8.txt -gnu-style-dependencies -MM -Mo "src/hello.d" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


