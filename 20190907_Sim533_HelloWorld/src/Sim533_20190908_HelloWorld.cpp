/*****************************************************************************
 * Sim533_20190908_HelloWorld.cpp
 *****************************************************************************/

#include <sys/platform.h>
#include "adi_initialize.h"
#include "Sim533_20190908_HelloWorld.h"
#include <stdio.h>
#include "../../ENCM511_SpecificFiles/ENCM511_include/CoffeePot_SimulatorFunctions2017.h"
#include "../../ENCM511_SpecificFiles/ENCM511_include/CoffeePot_SimulatorStructures2017.h"

extern "C" unsigned int MultiplyTwoNumbersLessThan10(unsigned short int ParamOne, unsigned short int ParamTwo);
/** 
 * If you want to use command program arguments, then place them in the following string. 
 */
char __argv_string[] = "";

int main(int argc, char *argv[])
{
	/**
	 * Initialize managed drivers and/or services that have been added to 
	 * the project.
	 * @return zero on success 
	 */
	adi_initComponents();
	
	/* Begin adding your custom code here */

	unsigned int temp = MultiplyTwoNumbersLessThan10(10,3);

	printf("BF533 Hello Simon.%i \n", temp);
	return 0;
}

