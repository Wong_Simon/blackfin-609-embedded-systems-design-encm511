.section program;
.global _MultiplyTwoNumbersLessThan10;

#define ParamOne_R0 R0
#define ParamTwo_R1 R1

_MultiplyTwoNumbersLessThan10:
Link 20;
	R2 = 10; 
	CC = ParamOne_R0<R2;
	if !CC JUMP JustEnd;
	
	CC = ParamTwo_R1<R2;
	if !CC JUMP JustEnd;
	
	ParamOne_R0 = ParamOne_R0 + ParamTwo_R1;
	Jump EndingNow;
JustEnd:
	R0 = 0xFFFFFFFF;
EndingNow:
Unlink;
_MultiplyTwoNumbersLessThan10.END:
RTS;