/*************************************************************************************
*   AUTOMATICALLY GENERATED COMMENT -- DO NOT MODIFY
* Author: Simon
* Date: Thu 2019/11/21 at 10:15:27 AM
* File Type: EUNIT Test File
*************************************************************************************/

#define EMBEDDEDUNIT_LITE
#include <EmbeddedUnit2017/EmbeddedUnit2017.h>
#include "TDD_Core_Timer.h"


TEST_CONTROL(TDD_Core_Timer_cpp);

#if 1
void UpdateEunitGui(void);
TEST(TDD_Core_Timer_cpp_GUIUpdate) {
	UpdateEunitGui();  // Conditionally compile this line (use #if 0) to stop an GUI update based on last completed test
}
#endif

//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////

TEST(CoreTimerInterrupt)
{
	My_SetUpCoreTimerInterrupts();
	My_Enable_CoreTimerInterrupts();
	My_Init_CoreTimer(PERIOD, INIT_COUNT);
	My_Start_CoreTimer();


	while ( ISR_Count_test < 10 ) {
		static unsigned long long int FirstCyclesValue = ReadCycles_ASM();
		if (My_Done_CoreTimer()){
			unsigned long long int SecondCyclesValue = ReadCycles_ASM();
			CHECK_CLOSE(PERIOD, SecondCyclesValue-FirstCyclesValue, 0.01*PERIOD);

			//Setup next iteration
			FirstCyclesValue = SecondCyclesValue;
		}
	}
	My_Stop_CoreTimer();
	My_Disable_CoreTimerInterrupts();
}


TEST(CoreTimerLoopWithPolling)
{
	My_Init_CoreTimer(PERIOD, INIT_COUNT);
	My_Start_CoreTimer();

	for(unsigned short int i = 0; i < 10;){
		static unsigned long long int FirstCyclesValue = ReadCycles_ASM();
		if (My_Done_CoreTimer()){
			unsigned long long int SecondCyclesValue = ReadCycles_ASM();
			CHECK_CLOSE(PERIOD, SecondCyclesValue-FirstCyclesValue, 0.01*PERIOD);

			//Setup next iteration
			FirstCyclesValue = SecondCyclesValue;
			i++;
		}
	}
	My_Stop_CoreTimer();
}


TEST(CoreTimerLoopTestWaitOnCoreTimer)
{
	My_Init_CoreTimer(PERIOD, INIT_COUNT);
	My_Start_CoreTimer();
	for(unsigned short int i = 0; i < 10; i++){
		static unsigned long long int FirstCyclesValue = ReadCycles_ASM();
		My_TimedWaitOnCoreTimer();
		unsigned long long int SecondCyclesValue = ReadCycles_ASM();
		CHECK_CLOSE(PERIOD, SecondCyclesValue-FirstCyclesValue, 0.01*PERIOD);

		//Setup next iteration
		FirstCyclesValue = SecondCyclesValue;
	}
}



TEST(StubPrinter)
{
	My_Init_CoreTimer(PERIOD, INIT_COUNT);
	My_Start_CoreTimer();
	My_Stop_CoreTimer();
	XF_CHECK(My_Done_CoreTimer());

	XF_CHECK(My_ReadCoreTimerRegister(DONT_KNOW) == 0);

	My_WriteCoreTimerRegister(DONT_KNOW, 0);


	My_Disable_CoreTimerInterrupts();
	My_Enable_CoreTimerInterrupts();
	My_SetUpCoreTimerInterrupts();
	My_Register_CoreTimerISR();
}



TEST_FILE_RUN_NOTIFICATION(TDD_Core_Timer_cpp);

