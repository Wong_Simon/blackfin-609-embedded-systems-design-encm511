################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
ASM_SRCS += \
../src/ReadCycles_ASM.asm 

CPP_SRCS += \
../src/Coffeepot_Sim533_CTimer_EUNIT2017_main.cpp \
../src/CoreTimer_Functions.cpp \
../src/ISR_GeneralCode.cpp \
../src/TDD_Core_Timer.cpp 

SRC_OBJS += \
./src/Coffeepot_Sim533_CTimer_EUNIT2017_main.doj \
./src/CoreTimer_Functions.doj \
./src/ISR_GeneralCode.doj \
./src/ReadCycles_ASM.doj \
./src/TDD_Core_Timer.doj 

ASM_DEPS += \
./src/ReadCycles_ASM.d 

CPP_DEPS += \
./src/Coffeepot_Sim533_CTimer_EUNIT2017_main.d \
./src/CoreTimer_Functions.d \
./src/ISR_GeneralCode.d \
./src/TDD_Core_Timer.d 


# Each subdirectory must supply rules for building sources it contributes
src/Coffeepot_Sim533_CTimer_EUNIT2017_main.doj: ../src/Coffeepot_Sim533_CTimer_EUNIT2017_main.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn -c -file-attr ProjectName="Coffeepot_Sim533_CTimer" -proc ADSP-BF533 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG @includes-70f15f36833f0be454240fbeeafbedb4.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/Coffeepot_Sim533_CTimer_EUNIT2017_main.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/CoreTimer_Functions.doj: ../src/CoreTimer_Functions.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn -c -file-attr ProjectName="Coffeepot_Sim533_CTimer" -proc ADSP-BF533 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG @includes-70f15f36833f0be454240fbeeafbedb4.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/CoreTimer_Functions.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/ISR_GeneralCode.doj: ../src/ISR_GeneralCode.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn -c -file-attr ProjectName="Coffeepot_Sim533_CTimer" -proc ADSP-BF533 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG @includes-70f15f36833f0be454240fbeeafbedb4.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/ISR_GeneralCode.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/ReadCycles_ASM.doj: ../src/ReadCycles_ASM.asm
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin Assembler'
	easmblkfn -file-attr ProjectName="Coffeepot_Sim533_CTimer" -proc ADSP-BF533 -si-revision any -g -DCORE0 -D_DEBUG @includes-28cd16c8209b68c1fac3958ba95e7097.txt -gnu-style-dependencies -MM -Mo "src/ReadCycles_ASM.d" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/TDD_Core_Timer.doj: ../src/TDD_Core_Timer.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn -c -file-attr ProjectName="Coffeepot_Sim533_CTimer" -proc ADSP-BF533 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG @includes-70f15f36833f0be454240fbeeafbedb4.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/TDD_Core_Timer.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


