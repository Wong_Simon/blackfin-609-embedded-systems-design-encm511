################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
C:/Analog\ Devices/CrossCore\ Embedded\ Studio\ 2.9.0/Blackfin/lib/src/services/source/tmr/adi_ctmr.c \
C:/Analog\ Devices/CrossCore\ Embedded\ Studio\ 2.9.0/Blackfin/lib/src/services/source/tmr/adi_tmr.c 

SRC_OBJS += \
./system/services/tmr/adi_ctmr.doj \
./system/services/tmr/adi_tmr.doj 

C_DEPS += \
./system/services/tmr/adi_ctmr.d \
./system/services/tmr/adi_tmr.d 


# Each subdirectory must supply rules for building sources it contributes
system/services/tmr/adi_ctmr.doj: C:/Analog\ Devices/CrossCore\ Embedded\ Studio\ 2.9.0/Blackfin/lib/src/services/source/tmr/adi_ctmr.c
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn -c -file-attr ProjectName="Coffeepot_Sim533_CTimer" -proc ADSP-BF533 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG @includes-70f15f36833f0be454240fbeeafbedb4.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "system/services/tmr/adi_ctmr.d" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

system/services/tmr/adi_tmr.doj: C:/Analog\ Devices/CrossCore\ Embedded\ Studio\ 2.9.0/Blackfin/lib/src/services/source/tmr/adi_tmr.c
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn -c -file-attr ProjectName="Coffeepot_Sim533_CTimer" -proc ADSP-BF533 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG @includes-70f15f36833f0be454240fbeeafbedb4.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "system/services/tmr/adi_tmr.d" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


