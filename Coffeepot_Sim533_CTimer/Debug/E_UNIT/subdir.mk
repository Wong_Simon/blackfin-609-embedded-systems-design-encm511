################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../E_UNIT/__EUNIT2017_TestControl_Coffeepot_Sim533_CTimer.cpp 

SRC_OBJS += \
./E_UNIT/__EUNIT2017_TestControl_Coffeepot_Sim533_CTimer.doj 

CPP_DEPS += \
./E_UNIT/__EUNIT2017_TestControl_Coffeepot_Sim533_CTimer.d 


# Each subdirectory must supply rules for building sources it contributes
E_UNIT/__EUNIT2017_TestControl_Coffeepot_Sim533_CTimer.doj: ../E_UNIT/__EUNIT2017_TestControl_Coffeepot_Sim533_CTimer.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn -c -file-attr ProjectName="Coffeepot_Sim533_CTimer" -proc ADSP-BF533 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG @includes-70f15f36833f0be454240fbeeafbedb4.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "E_UNIT/__EUNIT2017_TestControl_Coffeepot_Sim533_CTimer.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


