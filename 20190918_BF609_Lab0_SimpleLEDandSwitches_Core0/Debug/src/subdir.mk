################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
ASM_SRCS += \
../src/Lab1_GeneralCode_PORTF_ASM.asm \
../src/ReadProcessorCyclesASM.asm 

CPP_SRCS += \
../src/20190918_BF609_Lab0_Core0.cpp \
../src/20190918_Lab0_GeneralCode.cpp \
../src/Lab1_GeneralCode_PORTF.cpp \
../src/ReadProcessorCyclesCPP.cpp 

SRC_OBJS += \
./src/20190918_BF609_Lab0_Core0.doj \
./src/20190918_Lab0_GeneralCode.doj \
./src/Lab1_GeneralCode_PORTF.doj \
./src/Lab1_GeneralCode_PORTF_ASM.doj \
./src/ReadProcessorCyclesASM.doj \
./src/ReadProcessorCyclesCPP.doj 

ASM_DEPS += \
./src/Lab1_GeneralCode_PORTF_ASM.d \
./src/ReadProcessorCyclesASM.d 

CPP_DEPS += \
./src/20190918_BF609_Lab0_Core0.d \
./src/20190918_Lab0_GeneralCode.d \
./src/Lab1_GeneralCode_PORTF.d \
./src/ReadProcessorCyclesCPP.d 


# Each subdirectory must supply rules for building sources it contributes
src/20190918_BF609_Lab0_Core0.doj: ../src/20190918_BF609_Lab0_Core0.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn -c -file-attr ProjectName="20190918_BF609_Lab0_SimpleLEDandSwitches_Core0" -proc ADSP-BF609 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG -DADI_MCAPI @includes-1dd5e357c3577c45ac7da59deacb0542.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/20190918_BF609_Lab0_Core0.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/20190918_Lab0_GeneralCode.doj: ../src/20190918_Lab0_GeneralCode.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn -c -file-attr ProjectName="20190918_BF609_Lab0_SimpleLEDandSwitches_Core0" -proc ADSP-BF609 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG -DADI_MCAPI @includes-1dd5e357c3577c45ac7da59deacb0542.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/20190918_Lab0_GeneralCode.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/Lab1_GeneralCode_PORTF.doj: ../src/Lab1_GeneralCode_PORTF.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn -c -file-attr ProjectName="20190918_BF609_Lab0_SimpleLEDandSwitches_Core0" -proc ADSP-BF609 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG -DADI_MCAPI @includes-1dd5e357c3577c45ac7da59deacb0542.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/Lab1_GeneralCode_PORTF.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/Lab1_GeneralCode_PORTF_ASM.doj: ../src/Lab1_GeneralCode_PORTF_ASM.asm
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin Assembler'
	easmblkfn -file-attr ProjectName="20190918_BF609_Lab0_SimpleLEDandSwitches_Core0" -proc ADSP-BF609 -si-revision any -g -DCORE0 -D_DEBUG -DADI_MCAPI @includes-0e4ccbcf9b47b1e98bb233e597fe4d58.txt -gnu-style-dependencies -MM -Mo "src/Lab1_GeneralCode_PORTF_ASM.d" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/ReadProcessorCyclesASM.doj: ../src/ReadProcessorCyclesASM.asm
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin Assembler'
	easmblkfn -file-attr ProjectName="20190918_BF609_Lab0_SimpleLEDandSwitches_Core0" -proc ADSP-BF609 -si-revision any -g -DCORE0 -D_DEBUG -DADI_MCAPI @includes-0e4ccbcf9b47b1e98bb233e597fe4d58.txt -gnu-style-dependencies -MM -Mo "src/ReadProcessorCyclesASM.d" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/ReadProcessorCyclesCPP.doj: ../src/ReadProcessorCyclesCPP.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn -c -file-attr ProjectName="20190918_BF609_Lab0_SimpleLEDandSwitches_Core0" -proc ADSP-BF609 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG -DADI_MCAPI @includes-1dd5e357c3577c45ac7da59deacb0542.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/ReadProcessorCyclesCPP.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


