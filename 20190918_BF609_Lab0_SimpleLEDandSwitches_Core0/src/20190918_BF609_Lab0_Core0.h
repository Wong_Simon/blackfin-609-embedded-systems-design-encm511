/*****************************************************************************
 * 20190918_BF609_Lab0_Core0.h
 *****************************************************************************/

#ifndef __20190918_BF609_LAB0_CORE0_H__
#define __20190918_BF609_LAB0_CORE0_H__

/* Add your custom header content here */
#include <stdio.h>

//Defined values
#define GARBAGE_VALUE -1
#define SECOND_ONE 500000000
#define MILLISECOND_ONEHUNDRED 50000000
#define MILLISECOND_TEN 5000000
#define END_OF_LED_COMMANDS 9
#define END_OF_REB_READ_ARRAY 8
#define SWITCH_NONE 0x00
#define SWITCH_ONE 0x01
#define SWITCH_TWO 0x02
#define SWITCH_THREE 0x04
#define SWITCH_FOUR 0x08
#define SWITCH_FIVE 0x10
#define ON true
#define OFF false
#define ILLEGAL_MASK_LEFTRIGHT 0x500
#define ILLEGAL_MASK_FORWARDBACK 0xa00

//Misc. Prototypes
short int My_ReadSwitches(void);
void My_WriteLED(short int WriteLEDValue);
short int My_ReadLED(void);

void My_PrintBinaryValue(short int IntToBeConverted);
short int SwitchLower5BitMask(short int SwitchValueToMask);
void WaitClkCycles(unsigned long int CountTimer);

void PrintNumberofREBCommands(unsigned short int * REB_ReadArray);

short int My_ReadSwitches_10ms_debounce(short int PreviousSwitchValue);

//LAB0 Prototypes
extern "C" unsigned long int ReadProcessorCyclesASM(void);
extern "C" void ResetProcessorCyclesToZeroASM(void);
unsigned long int ReadProcessorCyclesCPP(void);
void ResetProcessorCyclestoZeroCPP(void);
void Start_Lab(void);

//LAB0 Inits
void My_Init_SwitchInterface(void);
void My_Init_LED_Interface(void);

//LAB1 CPP initializations
unsigned short int My_Read_GPIO_REB_Input(void);
void My_Write_GPIO_REB_Output (unsigned short int DataToWrite_16bits);
void My_Init_GPIO_REB_Input(void);
void My_Init_GPIO_REB_Output (void);

//LAB1 ASM initializations
extern "C" unsigned short int My_Read_GPIO_REB_Input_ASM(void);
extern "C" void My_Write_GPIO_REB_Output_ASM (unsigned short int DataToWrite_16bits);
extern "C" void My_Init_GPIO_REB_Input_ASM (void);
extern "C" void My_Init_GPIO_REB_Output_ASM (void);


#endif /* __20190918_BF609_LAB0_CORE0_H__ */
