/*****************************************************************************
 * 20190918_BF609_Lab0_Core0.cpp
 *****************************************************************************/

#include "20190918_BF609_Lab0_Core0.h"

#include <sys/platform.h>
#include <sys/adi_core.h>
#include <ccblkfn.h>
#include "adi_initialize.h"


char __argv_string[] = "";

int main(int argc, char *argv[])
{
	adi_initComponents();
	

#ifdef __ADSPBF533__
	printf("Start BF533 Lab\n");
	Start_Lab();
#endif


#ifdef __ADSPBF609__
	adi_core_enable(ADI_CORE_1);
	printf("Start BF609 Lab\n");
	Start_Lab();
#endif

	printf("Lab Complete\n");
	return 0;

}

