/*****************************************************************************
 * 20190908_Sim533_Lab0.cpp
 *****************************************************************************/

#include <sys/platform.h>
#include "adi_initialize.h"
#include "20190908_Sim533_Lab0.h"

/** 
 * If you want to use command program arguments, then place them in the following string. 
 */
char __argv_string[] = "";

int main(int argc, char *argv[])
{
	/**
	 * Initialize managed drivers and/or services that have been added to 
	 * the project.
	 * @return zero on success 
	 */
	adi_initComponents();
	
	/* Begin adding your custom code here */

#ifdef __ADSPBF533__
	printf("Start BF533 Lab 0\n");
	Start_Lab0();
#endif

	printf("Lab 0 Complete\n");
	return 0;
}

