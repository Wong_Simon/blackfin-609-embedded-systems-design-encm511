/*
 * 20190908_Lab0_GeneralCode.cpp
 *
 *  Created on: Sep 8, 2019
 *      Author: simon
 */

//Defined values
#define GARBAGE_VALUE -1

//Headers
#include <sys/platform.h>
#include "adi_initialize.h"
#include "20190908_Sim533_Lab0.h"

//Prototypes
void My_Init_SwitchInterface(void);
void My_Init_LED_Interface(void);

short int My_ReadSwitches(void);
void My_WriteLED(short int WriteLEDValue);
short int My_ReadLED(void);

void My_PrintBinaryValue(short int IntToBeConverted);

//Private global variable
static bool Init_Switch_Interface_Done = false;
static bool Init_LED_Interface_Done = false;
static short int LastLEDValueWritten = GARBAGE_VALUE;

//Primary stub
void Start_Lab0(void) { //This is a CODE STUB - term required for marked portions
	printf("Stub for Start_Lab0\n");

	//Initialization of hardware
	My_Init_SwitchInterface();
	My_Init_LED_Interface();

	// Read switches
	printf("Press Switch 2\n");
	short int switch_value = My_ReadSwitches();
	if (switch_value != 2) {	//Read switch test
		printf("Test Fail: switch_value / ReadSwitches\n");
	}


#if 0 //Testing Write LED value with solid bit pattern
	short int use_LED_value = 0xCC;
	My_WriteLED(use_LED_value);
#endif

#if 0 //Testing Write LED value with a ascii heart
	short int temp;
	short int heart[9] = {0x00, 0x78, 0xfc, 0xfe, 0x7f, 0xfe, 0xfc, 0x78, 0x00};

	for (temp=0; temp<9; temp++) {
		My_WriteLED(heart[temp]);
	}
#endif

#if 1 //Testing Write LED value using my initials
	short int temp;
	short int sw[13] = {0x87, 0x89, 0xf1, //Letter S
			0x00, 0xc0, 0x00, //space dot space
			0xff, 0x40, 0x20, 0x40, 0xff, //letter W
			0x00, 0xc0};

	for (temp=0; temp<13; temp++) {
		My_WriteLED(sw[temp]);
	}
#endif

	//Read LED value
	short int read_LED_value = My_ReadLED();
	if (read_LED_value != LastLEDValueWritten) { //Read LED test
		printf("Test Fail: read_LED_value / ReadLED\n");
	}

	printf("Exiting Start_Lab0\n");

}


//Aux. stubs
void My_Init_SwitchInterface(void){
	printf("Stub for Init_Switch_Interface\n");
	Init_Switch_Interface_Done = true;
}


void My_Init_LED_Interface(void){
	printf("Stub for Init_LED_Interface\n");
	Init_LED_Interface_Done = true;
}


short int My_ReadSwitches(void){
	if (Init_Switch_Interface_Done) {
		printf("Stub Pass: ReadSwitches\n");
		return 0xCC; //random variable
	}
	printf("Stub Fail: ReadSwitches - Switch Hardware not initialized\n");
	return GARBAGE_VALUE;
}


void My_WriteLED(short int WriteLEDValue){
	if (Init_LED_Interface_Done) {
		//printf("Stub Pass: WriteLED\n");
		My_PrintBinaryValue(WriteLEDValue);
		LastLEDValueWritten = WriteLEDValue;
		return; //Exit
	}

	//Else
	printf("Stub Fail: WriteLED - LED Hardware not initialized\n");
}


short int My_ReadLED(void){
	if (Init_LED_Interface_Done) {
		printf("Stub Pass: ReadLED\n");
		return LastLEDValueWritten; //random variable
	}

	//Else
	printf("Stub Fail: ReadLED - LED Hardware not initialized\n");
	return GARBAGE_VALUE;
}

void My_PrintBinaryValue(short int IntToBeConverted){
	//printf("Stub for PrintLEDValue\n");
	char BitPattern[17] = {' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ', 0x0};
	unsigned int TempBitPatternIndex = (sizeof(BitPattern)/sizeof(BitPattern[0])) - 2; //Get last index of BitPattern

	int TempIntValue= IntToBeConverted;
	while(TempIntValue != 0) {
		BitPattern[TempBitPatternIndex] = (TempIntValue%2 != 0) ? '1' : ' ';
		TempBitPatternIndex = TempBitPatternIndex - 1;
		TempIntValue = TempIntValue/2;
	}

	printf("PBVfI - Decimal %3i - Hex 0x%2X - Binary %s\n", IntToBeConverted, IntToBeConverted, BitPattern);
}
