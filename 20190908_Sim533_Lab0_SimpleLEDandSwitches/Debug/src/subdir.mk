################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/20190908_Lab0_GeneralCode.cpp \
../src/20190908_Sim533_Lab0.cpp 

SRC_OBJS += \
./src/20190908_Lab0_GeneralCode.doj \
./src/20190908_Sim533_Lab0.doj 

CPP_DEPS += \
./src/20190908_Lab0_GeneralCode.d \
./src/20190908_Sim533_Lab0.d 


# Each subdirectory must supply rules for building sources it contributes
src/20190908_Lab0_GeneralCode.doj: ../src/20190908_Lab0_GeneralCode.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn -c -file-attr ProjectName="20190908_Sim533_Lab0_SimpleLEDandSwitches" -proc ADSP-BF533 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG @includes-0ad3bc8ab79ff9c42172a92fcc4ff80c.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/20190908_Lab0_GeneralCode.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/20190908_Sim533_Lab0.doj: ../src/20190908_Sim533_Lab0.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn -c -file-attr ProjectName="20190908_Sim533_Lab0_SimpleLEDandSwitches" -proc ADSP-BF533 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG @includes-0ad3bc8ab79ff9c42172a92fcc4ff80c.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/20190908_Sim533_Lab0.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


