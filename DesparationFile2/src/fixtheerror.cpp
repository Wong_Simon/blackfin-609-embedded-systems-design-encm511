/*************************************************************************************
*   AUTOMATICALLY GENERATED COMMENT -- DO NOT MODIFY
* Author: Simon
* Date: Fri 2019/11/22 at 11:29:30 PM
* File Type: EUNIT Test File
*************************************************************************************/

#define EMBEDDEDUNIT_LITE
#include <EmbeddedUnit2017/EmbeddedUnit2017.h>
//#include "fixtheerror_cpp.h"
#include "TDD_Core_Timer.h"

TEST_CONTROL(fixtheerror_cpp);

#if 1
void UpdateEunitGui(void);
TEST(fixtheerror_cpp_GUIUpdate) {
	UpdateEunitGui();  // Conditionally compile this line (use #if 0) to stop an GUI update based on last completed test
}
#endif


TEST(CoreTimerLoopWithPolling)
{
	My_Init_CoreTimer(PERIOD, INIT_COUNT);

	My_Start_CoreTimer();
	for(unsigned short int i = 0; i < 10;){
		static unsigned long long int FirstCyclesValue = ReadCycles_ASM();
		if (My_Done_CoreTimer()){
			unsigned long long int SecondCyclesValue = ReadCycles_ASM();
			MY_CHECK_CLOSE(PERIOD, SecondCyclesValue-FirstCyclesValue, 0.01);

			//Setup next iteration
			FirstCyclesValue = SecondCyclesValue;
			i++;
		}
	}
}

TEST(CoreTimerLoopTestWaitOnCoreTimer)
{
	My_Init_CoreTimer(PERIOD, INIT_COUNT);

	My_Start_CoreTimer();
	for(unsigned short int i = 0; i < 10; i++){
		unsigned long long int FirstCyclesValue = ReadCycles_ASM();
		My_TimedWaitOnCoreTimer();
		unsigned long long int SecondCyclesValue = ReadCycles_ASM();
		MY_CHECK_CLOSE(PERIOD, SecondCyclesValue-FirstCyclesValue, 0.01);
	}
}

TEST(TestCheckCloseFix)
{
	MY_CHECK_CLOSE(100,104,0.05); //Expect to pass
}


TEST(StubPrinter)
{
	My_Init_CoreTimer(PERIOD, INIT_COUNT);
	My_Start_CoreTimer();
	My_Stop_CoreTimer();

	//unsigned long int tempTestVar_1 = My_Read_CoreTimer();
	XF_CHECK(My_Read_CoreTimer() == 0);

	//bool tempTestVar_2 = My_Done_CoreTimer();
	XF_CHECK(My_Done_CoreTimer());
	My_Write_CoreTimer();

	//bool tempTestVar_3 = My_CoreTimer_My_Main();
	XF_CHECK(My_CoreTimer_My_Main());


	//unsigned long int tempTestVar_4 = My_PlannedCoreTimerRegisterValue(DONT_KNOW);
	XF_CHECK(My_PlannedCoreTimerRegisterValue(DONT_KNOW) == 0);

	//unsigned long int tempTestVar_5 = My_ReadCoreTimerRegister(DONT_KNOW);
	XF_CHECK(My_ReadCoreTimerRegister(DONT_KNOW) == 0);

	My_WriteCoreTimerRegister(DONT_KNOW, 0);



	My_Disable_CoreTimerInterrupts();
	My_Enable_CoreTimerInterrupts();
	My_SetUpCoreTimerInterrupts();
	My_Register_CoreTimerISR();
}


void MY_CHECK_CLOSE(unsigned long long int expected, unsigned long long int actual, double tolerance_percent_between_0n1){
	unsigned long long int expected_low = expected * (1.0 - tolerance_percent_between_0n1);
	unsigned long long int expected_high = expected * (1.0 + tolerance_percent_between_0n1);
	//CHECK((actual >= expected_low) && (actual <= expected_high));

}




TEST_FILE_RUN_NOTIFICATION(fixtheerror_cpp);



