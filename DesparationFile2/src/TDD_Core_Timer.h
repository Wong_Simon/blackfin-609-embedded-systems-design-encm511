/*************************************************************************************
* AUTO-GENERATED COMMENT - DO NOT MODIFY 
* Author: Simon
* Date: Thu 2019/11/21 at 10:15:27 AM
* File Type: EUNIT Test Header File
*************************************************************************************/

#ifndef TDD_CORE_TIMER_H
#define TDD_CORE_TIMER_H

#define TINT_BIT 0x8
#define AUTORELOAD_BIT 0x4
#define TMPWR_BIT 0x1
#define TMREN_BIT 0x2
#define FIRST4BITS 0xF

#include "../../ENCM511_SpecificFiles/ENCM511_include/CoreTimer_Library.h"
#include <stdio.h>
#include <blackfin.h>
void UpdateEunitGui(void);	// Update EUNIT GUI with results from previous test

void MY_CHECK_CLOSE(unsigned long long int expected, unsigned long long int actual, double tolerance_between_0n1);

extern "C" unsigned long long int ReadCycles_ASM(void);

void My_Init_CoreTimer(unsigned long int tperiod, unsigned long int tcount);
void My_Start_CoreTimer(void);
void My_Stop_CoreTimer(void);
unsigned long int My_Read_CoreTimer(void);
bool My_Done_CoreTimer(void);
void My_Write_CoreTimer(void); //unused
void My_TimedWaitOnCoreTimer(void);

bool My_CoreTimer_My_Main(void);

void My_Disable_CoreTimerInterrupts(void);
void My_Enable_CoreTimerInterrupts(void);
void My_SetUpCoreTimerInterrupts(void);
void My_Register_CoreTimerISR(void);


//Read the registers
enum CORETIMER_REG {DONT_KNOW, REG_COUNTER, REG_PERIOD, REG_TSCALE, REG_CNTRL};
unsigned long int My_PlannedCoreTimerRegisterValue(CORETIMER_REG regName);
unsigned long int My_ReadCoreTimerRegister(CORETIMER_REG regName);
void My_WriteCoreTimerRegister(CORETIMER_REG regName, unsigned long int newValue);




#define SHOW_DEBUG_CONSOLE_INFORMATION true
inline void Show_Debug_Console_Information(char *DebugInfo){
	if (SHOW_DEBUG_CONSOLE_INFORMATION){
		printf("%s", DebugInfo);
	}
}

#endif
