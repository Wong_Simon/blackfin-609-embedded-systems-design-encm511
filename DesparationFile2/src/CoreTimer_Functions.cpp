/*
 * CoreTimer_Functions.cpp
 *
 *  Created on: Nov 22, 2019
 *      Author: Simon
 */

#include "TDD_Core_Timer.h"


void My_Init_CoreTimer(unsigned long int tperiod, unsigned long int tcount){
	Show_Debug_Console_Information("DEBUG: Stub My_Init_CoreTimer\n");

	My_WriteCoreTimerRegister(REG_PERIOD, tperiod);
	My_WriteCoreTimerRegister(REG_COUNTER, tcount);
	My_WriteCoreTimerRegister(REG_TSCALE, 0);
	My_WriteCoreTimerRegister(REG_CNTRL, (My_ReadCoreTimerRegister(REG_CNTRL) & ~FIRST4BITS) | AUTORELOAD_BIT | TMPWR_BIT);

}

void My_Start_CoreTimer(void){
	Show_Debug_Console_Information("DEBUG: Stub My_Start_CoreTimer\n");

	My_WriteCoreTimerRegister(REG_CNTRL, My_ReadCoreTimerRegister(REG_CNTRL) | TMREN_BIT);
}

void My_Stop_CoreTimer(void){
	Show_Debug_Console_Information("DEBUG: Stub My_Stop_CoreTimer\n");

	My_WriteCoreTimerRegister(REG_CNTRL, My_ReadCoreTimerRegister(REG_CNTRL) & ~TMREN_BIT);
}

//unused
unsigned long int My_Read_CoreTimer(void){
	Show_Debug_Console_Information("DEBUG: Stub My_Read_CoreTimer\n");
	return -1;
}


bool My_Done_CoreTimer(void){
	Show_Debug_Console_Information("DEBUG: Stub My_Done_CoreTimer\n");

	if ( ( My_ReadCoreTimerRegister(REG_CNTRL) & TINT ) == TINT ){
		//Assuming I am using auto-reload, so need to clear the TINT register, dont need to feed dog
		unsigned long int tempREG_CNTRL = My_ReadCoreTimerRegister(REG_CNTRL);
		My_WriteCoreTimerRegister(REG_CNTRL, tempREG_CNTRL & (~TINT_BIT)); //Clear TINT sticky bit

		return true;
	}
	return false;
}

//unused
void My_Write_CoreTimer(void){
	Show_Debug_Console_Information("DEBUG: Stub My_Write_CoreTimer\n");
}

void My_TimedWaitOnCoreTimer(void){
	Show_Debug_Console_Information("DEBUG: Stub My_Write_CoreTimer\n");
}


//unused probably
bool My_CoreTimer_My_Main(void){
	Show_Debug_Console_Information("DEBUG: Stub My_CoreTimer_My_Main\n");
	return false;
}



//unused
unsigned long int My_PlannedCoreTimerRegisterValue(CORETIMER_REG regName){
	Show_Debug_Console_Information("DEBUG: Stub My_PlannedCoreTimerRegisterValue\n");
	return -1;
}

unsigned long int My_ReadCoreTimerRegister(CORETIMER_REG regName){
	Show_Debug_Console_Information("DEBUG: Stub My_ReadCoreTimerRegister\n");
	unsigned long int ReturnValue = (unsigned long int) 0xFFFFFFFF;
	switch(regName){
		case REG_COUNTER: ReturnValue = *pTCOUNT;
		break;

		case REG_PERIOD: ReturnValue = *pTPERIOD;
		break;

		case REG_TSCALE: ReturnValue = *pTSCALE;
		break;

		case REG_CNTRL: ReturnValue = *pTCNTL;
		break;
	}

	return ReturnValue;
}

void My_WriteCoreTimerRegister(CORETIMER_REG regName, unsigned long int newValue){
	Show_Debug_Console_Information("DEBUG: Stub My_WriteCoreTimerRegister\n");
	switch(regName){
			case REG_COUNTER: *pTCOUNT = newValue;
			break;

			case REG_PERIOD: *pTPERIOD = newValue;
			break;

			case REG_TSCALE: *pTSCALE = newValue;
			break;

			case REG_CNTRL: *pTCNTL = newValue;
			break;
		}
	}




//WSIC
void My_Disable_CoreTimerInterrupts(void){
	Show_Debug_Console_Information("DEBUG: Stub My_Disable_CoreTimerInterrupts\n");
}

//WSIC
void My_Enable_CoreTimerInterrupts(void){
	Show_Debug_Console_Information("DEBUG: Stub My_Enable_CoreTimerInterrupts\n");
}

//WSIC
void My_SetUpCoreTimerInterrupts(void){
	Show_Debug_Console_Information("DEBUG: Stub My_SetUpCoreTimerInterrupts\n");
}

//WSIC
void My_Register_CoreTimerISR(void){
	Show_Debug_Console_Information("DEBUG: Stub My_Register_CoreTimerISR\n");
}
