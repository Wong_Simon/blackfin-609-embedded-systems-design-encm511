################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
ASM_SRCS += \
../src/ReadCycles_ASM.asm 

CPP_SRCS += \
../src/CoreTimer_Functions.cpp \
../src/DesparationFile2_EUNIT2017_main.cpp \
../src/fixtheerror.cpp 

SRC_OBJS += \
./src/CoreTimer_Functions.doj \
./src/DesparationFile2_EUNIT2017_main.doj \
./src/ReadCycles_ASM.doj \
./src/fixtheerror.doj 

ASM_DEPS += \
./src/ReadCycles_ASM.d 

CPP_DEPS += \
./src/CoreTimer_Functions.d \
./src/DesparationFile2_EUNIT2017_main.d \
./src/fixtheerror.d 


# Each subdirectory must supply rules for building sources it contributes
src/CoreTimer_Functions.doj: ../src/CoreTimer_Functions.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn -c -file-attr ProjectName="DesparationFile2" -proc ADSP-BF533 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG @includes-27665d76466c2d81760d816e09c0805b.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/CoreTimer_Functions.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/DesparationFile2_EUNIT2017_main.doj: ../src/DesparationFile2_EUNIT2017_main.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn -c -file-attr ProjectName="DesparationFile2" -proc ADSP-BF533 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG @includes-27665d76466c2d81760d816e09c0805b.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/DesparationFile2_EUNIT2017_main.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/ReadCycles_ASM.doj: ../src/ReadCycles_ASM.asm
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin Assembler'
	easmblkfn -file-attr ProjectName="DesparationFile2" -proc ADSP-BF533 -si-revision any -g -DCORE0 -D_DEBUG @includes-393aa3d892129f044c90fa887499ea28.txt -gnu-style-dependencies -MM -Mo "src/ReadCycles_ASM.d" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/fixtheerror.doj: ../src/fixtheerror.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn -c -file-attr ProjectName="DesparationFile2" -proc ADSP-BF533 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG @includes-27665d76466c2d81760d816e09c0805b.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/fixtheerror.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


