/*
 * Lab2_REBThread.cpp
 *
 *  Created on: Nov 21, 2019
 *      Author: Simon
 */
#include "Lab2_REBThread.h"
#include "Lab2_BF609_UTTCOS_Core0_uTTCOSg2017_main.h"

#define END_OF_REB_LED_COUNTER 0xF

//extern volatile char ID_PWM_REB_LED_All = 0;
void PWM_REB_LED_All(void) {
	//Counts on the REB leds
	static short int CurrentLEDPattern = 0;
	CurrentLEDPattern =
			(CurrentLEDPattern > END_OF_REB_LED_COUNTER) ?
					0 : CurrentLEDPattern + 1;

	My_Write_GPIO_REB_Output_ASM(CurrentLEDPattern << 12);

}

extern volatile char ID_ReadREBSwitch_Thread = 0;
extern volatile unsigned short int REB_Read_Array[END_OF_REB_READ_ARRAY] = { 0 };
static short int REBRead_Index = 0;
void ReadREBSwitch_Thread(void) {

	REB_Read_Array[REBRead_Index] = My_Read_GPIO_REB_Input_ASM();

	REBRead_Index =
			(REBRead_Index >= END_OF_REB_READ_ARRAY - 1) ?
					REBRead_Index : REBRead_Index + 1;
}

extern volatile char ID_WriteREBLEDsfromReadArray = 0;
void WriteREBLEDsfromReadArray(void) {
	static short int REBWritefromRead_Index = 0;

	My_Write_GPIO_REB_Output_ASM(REB_Read_Array[REBWritefromRead_Index] << 4);

	if (REBWritefromRead_Index >= REBRead_Index - 1) {
		uTTCOSg_DeleteThread(ID_WriteREBLEDsfromReadArray);
		for (short int i = 0; i < REBRead_Index; i++) {
			REB_Read_Array[i] = 0;
		}
		REBWritefromRead_Index = 0;
		REBRead_Index = 0;
	}
	REBWritefromRead_Index =
			(REBWritefromRead_Index >= REBRead_Index - 1) ?
					0 : REBWritefromRead_Index + 1;
}
