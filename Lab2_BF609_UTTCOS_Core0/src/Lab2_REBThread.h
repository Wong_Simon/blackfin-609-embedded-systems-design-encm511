/*
 * Lab2_REBThread.h
 *
 *  Created on: Nov 21, 2019
 *      Author: Simon
 */

#ifndef LAB2_REBTHREAD_H_
#define LAB2_REBTHREAD_H_

#include <uTTCOSg2017/uTTCOSg.h>
#include <GPIO2017/ADSP_GPIO_interface.h>

#include "Lab2_FPThread.h"   // Should include links to other project header files

#include "GeneralCode_Lab0n1.h"

void PWM_REB_LED_All (void);
void ReadREBSwitch_Thread (void);
void WriteREBLEDsfromReadArray(void);

extern volatile char ID_ReadREBSwitch_Thread;
extern volatile char ID_WriteREBLEDsfromReadArray;

#endif /* LAB2_REBTHREAD_H_ */
