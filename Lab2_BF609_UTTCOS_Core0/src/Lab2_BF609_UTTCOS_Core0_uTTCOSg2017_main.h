/*************************************************************************************
* AUTO-GENERATED COMMENT - DO NOT MODIFY 
* Author: Simon
* Date: Tue 2019/11/19 at 01:46:48 PM
* File Type: uTTCOS Task Header File
*************************************************************************************/

#ifndef LAB2_BF609_UTTCOS_CORE0_UTTCOSG2017_H
#define LAB2_BF609_UTTCOS_CORE0_UTTCOSG2017_H

//Changes
#define END_OF_REB_READ_ARRAY 32




#include <uTTCOSg2017/uTTCOSg.h>
#include <GPIO2017/ADSP_GPIO_interface.h>
// extern "C" void BlackfinBF533_uTTCOSg_Audio_Rx_Tx_Task(void); 
extern "C" void SHARC21469_uTTCOSg_Audio_Rx_Tx_Task(void);
extern "C" void ADSP_SC589_uTTCOSg_Audio_Rx_Tx_Task(void);

// TODO -- Once you have demonstrated the idea of uTTCOS working with print statements
// Comment out the following include statement
// DON'T USE PRINT STATEMENT INSIDE uTTCOS as it is a real time system and
// print statements run on the HIGH priority emulator interrupt and disrupt real time operations

#include <stdio.h>

#include "Lab2_FPThread.h"
#include "Lab2_REBThread.h"
#include "LCD_SPI_GeneralFunctions.h"

#if defined(__ADSPBF609__)
#define  TIC_CONTROL_VALUE ((unsigned long int) 4800000)		// BF609 EMULATOR
#define TICS_PER_SECOND 	100
#define ONE_SECOND 			TICS_PER_SECOND		// If TICS_CONTROL_VALUE Adjusted correctly
#define RUN_ONCE			0
#define NO_DELAY			0
#else
#error "Unknown ADSP or ARM processor"
#endif



void Custom_uTTCOS_OS_Init(unsigned long int);
extern "C" void Idle_WaitForInterrupts_ASM(void);
void uTTCOSg_Start_CoreTimer_Scheduler(unsigned int maxNumberThreads);

/*
void Task_RemoveMeSoon_Print1(void);
void Task_RemoveMeSoon_Print2(void);
void Set_Up_NOT_START_RemoveMeSoonTasks(void);
void KillerOfPrintStatements_Hunting(void);
void KillerOfPrintStatements(void);
void TheEnd(void);

extern "C" unsigned long long int ReadProcessorCyclesASM(void);
*/

#endif
