/*
 * LCD_SPI_GeneralFunctions.h
 *
 *  Created on: Nov 27, 2019
 *      Author: Simon
 */

#ifndef LCD_SPI_GENERALFUNCTIONS_H_
#define LCD_SPI_GENERALFUNCTIONS_H_

//TEMPERSATURE / GP TIMR
#include "../../ENCM511_SpecificFiles/ENCM511_include/GP_Timer_Library.h"
#define GPTIMER3 3
#define ZERO_ASCII_ID 48

void Init_GP_Timer_Stuff(void);
unsigned long long int ReadTemperature (void);
unsigned char * toxArray(unsigned long long int number);

void SPI_MessageTemperature(void);
extern volatile char ID_SPI_MessageTemperature;

//extern volatile unsigned char numberArray[3];



//SPI
#include "../../ENCM511_SpecificFiles/ENCM511_include/ADSP_BF609_Utilities_Library.h"
#include "../../ENCM511_SpecificFiles/ENCM511_include/REB_SPI_Library.h"
#include "../../ENCM511_SpecificFiles/ENCM511_include/Board_LED_Switches.h"
#include "../../ENCM511_SpecificFiles/ENCM511_include/REB_GPIO_Output_Library.h"
#include <stdio.h>
#include <blackfin.h>

#define END_OF_SCREEN_INIT_ARRAY 4
#define END_OF_FIRSTMESAGE 10
#define E_BIT 0x0100
#define RW_BIT 0x0200
#define RS_BIT 0x0400
#define CLEAR_SCREEN_BIT 0x0001
#define TIC_DELAY 480000*1
void Init_SPI_Stuff (void);
void SPI_Write_ebitPulse (unsigned short int WriteVal, bool CommandMode);
void SPI_InitScreenArray (void);
void SPI_WriteMessageArray (unsigned char * StringWithNullChar, unsigned short int EndIndex_WriteMessage);
void SPI_ClearScreen (void);

extern volatile unsigned short int Screen_Init_Array [END_OF_SCREEN_INIT_ARRAY]; //remember that bit 10 need to be on from this point forward
extern volatile unsigned char FirstMessageArray[END_OF_FIRSTMESAGE];

extern volatile char ID_SPI_InitScreenArray;

extern volatile char ID_SPI_Message1;
void SPI_Message1(void);

extern volatile char ID_SPI_Message2;
void SPI_Message2(void);


//todo remove
void Waitasec(void);
#endif /* LCD_SPI_GENERALFUNCTIONS_H_ */
