################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
ASM_SRCS += \
../src/GeneralCode_Lab0n1_PORTF_ASM.asm \
../src/ReadProcessorCyclesASM.asm 

CPP_SRCS += \
../src/GeneralCode_Lab0n1.cpp \
../src/LCD_SPI_GeneralFunctions.cpp \
../src/Lab2_BF609_UTTCOS_Core0_uTTCOSg2017_main.cpp \
../src/Lab2_FPThread.cpp \
../src/Lab2_REBThread.cpp \
../src/ReadProcessorCyclesCPP.cpp \
../src/SetBoard_LED_SPI_SoftConfig_BF609.cpp \
../src/Set_Pinmux_config.cpp \
../src/faultyLED1_Thread.cpp 

SRC_OBJS += \
./src/GeneralCode_Lab0n1.doj \
./src/GeneralCode_Lab0n1_PORTF_ASM.doj \
./src/LCD_SPI_GeneralFunctions.doj \
./src/Lab2_BF609_UTTCOS_Core0_uTTCOSg2017_main.doj \
./src/Lab2_FPThread.doj \
./src/Lab2_REBThread.doj \
./src/ReadProcessorCyclesASM.doj \
./src/ReadProcessorCyclesCPP.doj \
./src/SetBoard_LED_SPI_SoftConfig_BF609.doj \
./src/Set_Pinmux_config.doj \
./src/faultyLED1_Thread.doj 

ASM_DEPS += \
./src/GeneralCode_Lab0n1_PORTF_ASM.d \
./src/ReadProcessorCyclesASM.d 

CPP_DEPS += \
./src/GeneralCode_Lab0n1.d \
./src/LCD_SPI_GeneralFunctions.d \
./src/Lab2_BF609_UTTCOS_Core0_uTTCOSg2017_main.d \
./src/Lab2_FPThread.d \
./src/Lab2_REBThread.d \
./src/ReadProcessorCyclesCPP.d \
./src/SetBoard_LED_SPI_SoftConfig_BF609.d \
./src/Set_Pinmux_config.d \
./src/faultyLED1_Thread.d 


# Each subdirectory must supply rules for building sources it contributes
src/GeneralCode_Lab0n1.doj: ../src/GeneralCode_Lab0n1.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn -c -file-attr ProjectName="Lab2_BF609_UTTCOS_Core0" -proc ADSP-BF609 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG -DADI_MCAPI @includes-020b629afcb80103996c4a6971e8a362.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/GeneralCode_Lab0n1.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/GeneralCode_Lab0n1_PORTF_ASM.doj: ../src/GeneralCode_Lab0n1_PORTF_ASM.asm
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin Assembler'
	easmblkfn -file-attr ProjectName="Lab2_BF609_UTTCOS_Core0" -proc ADSP-BF609 -si-revision any -g -DCORE0 -D_DEBUG -DADI_MCAPI @includes-75cb79323463fd0ae821e5192386968b.txt -gnu-style-dependencies -MM -Mo "src/GeneralCode_Lab0n1_PORTF_ASM.d" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/LCD_SPI_GeneralFunctions.doj: ../src/LCD_SPI_GeneralFunctions.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn -c -file-attr ProjectName="Lab2_BF609_UTTCOS_Core0" -proc ADSP-BF609 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG -DADI_MCAPI @includes-020b629afcb80103996c4a6971e8a362.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/LCD_SPI_GeneralFunctions.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/Lab2_BF609_UTTCOS_Core0_uTTCOSg2017_main.doj: ../src/Lab2_BF609_UTTCOS_Core0_uTTCOSg2017_main.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn -c -file-attr ProjectName="Lab2_BF609_UTTCOS_Core0" -proc ADSP-BF609 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG -DADI_MCAPI @includes-020b629afcb80103996c4a6971e8a362.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/Lab2_BF609_UTTCOS_Core0_uTTCOSg2017_main.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/Lab2_FPThread.doj: ../src/Lab2_FPThread.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn -c -file-attr ProjectName="Lab2_BF609_UTTCOS_Core0" -proc ADSP-BF609 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG -DADI_MCAPI @includes-020b629afcb80103996c4a6971e8a362.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/Lab2_FPThread.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/Lab2_REBThread.doj: ../src/Lab2_REBThread.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn -c -file-attr ProjectName="Lab2_BF609_UTTCOS_Core0" -proc ADSP-BF609 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG -DADI_MCAPI @includes-020b629afcb80103996c4a6971e8a362.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/Lab2_REBThread.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/ReadProcessorCyclesASM.doj: ../src/ReadProcessorCyclesASM.asm
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin Assembler'
	easmblkfn -file-attr ProjectName="Lab2_BF609_UTTCOS_Core0" -proc ADSP-BF609 -si-revision any -g -DCORE0 -D_DEBUG -DADI_MCAPI @includes-75cb79323463fd0ae821e5192386968b.txt -gnu-style-dependencies -MM -Mo "src/ReadProcessorCyclesASM.d" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/ReadProcessorCyclesCPP.doj: ../src/ReadProcessorCyclesCPP.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn -c -file-attr ProjectName="Lab2_BF609_UTTCOS_Core0" -proc ADSP-BF609 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG -DADI_MCAPI @includes-020b629afcb80103996c4a6971e8a362.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/ReadProcessorCyclesCPP.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/SetBoard_LED_SPI_SoftConfig_BF609.doj: ../src/SetBoard_LED_SPI_SoftConfig_BF609.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn -c -file-attr ProjectName="Lab2_BF609_UTTCOS_Core0" -proc ADSP-BF609 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG -DADI_MCAPI @includes-020b629afcb80103996c4a6971e8a362.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/SetBoard_LED_SPI_SoftConfig_BF609.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/Set_Pinmux_config.doj: ../src/Set_Pinmux_config.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn -c -file-attr ProjectName="Lab2_BF609_UTTCOS_Core0" -proc ADSP-BF609 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG -DADI_MCAPI @includes-020b629afcb80103996c4a6971e8a362.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/Set_Pinmux_config.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/faultyLED1_Thread.doj: ../src/faultyLED1_Thread.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn -c -file-attr ProjectName="Lab2_BF609_UTTCOS_Core0" -proc ADSP-BF609 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG -DADI_MCAPI @includes-020b629afcb80103996c4a6971e8a362.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/faultyLED1_Thread.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


