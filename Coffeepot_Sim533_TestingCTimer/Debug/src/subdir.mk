################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Coffeepot_Sim533_TestingCTimer_EUNIT2017_main.cpp \
../src/CoreTimer_Tests.cpp 

SRC_OBJS += \
./src/Coffeepot_Sim533_TestingCTimer_EUNIT2017_main.doj \
./src/CoreTimer_Tests.doj 

CPP_DEPS += \
./src/Coffeepot_Sim533_TestingCTimer_EUNIT2017_main.d \
./src/CoreTimer_Tests.d 


# Each subdirectory must supply rules for building sources it contributes
src/Coffeepot_Sim533_TestingCTimer_EUNIT2017_main.doj: ../src/Coffeepot_Sim533_TestingCTimer_EUNIT2017_main.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn -c -file-attr ProjectName="Coffeepot_Sim533_TestingCTimer" -proc ADSP-BF533 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG @includes-2adb1f21016e6ab76098e62f27d73763.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/Coffeepot_Sim533_TestingCTimer_EUNIT2017_main.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/CoreTimer_Tests.doj: ../src/CoreTimer_Tests.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn -c -file-attr ProjectName="Coffeepot_Sim533_TestingCTimer" -proc ADSP-BF533 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG @includes-2adb1f21016e6ab76098e62f27d73763.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/CoreTimer_Tests.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


