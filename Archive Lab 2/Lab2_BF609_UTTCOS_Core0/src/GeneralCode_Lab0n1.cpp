/*
 * 20190918_Lab0_GeneralCode.cpp
 *
 *  Created on: Sep 18, 2019
 *      Author: Simon Wong and Kevin Blakey
 */

//Headers
#include <sys/platform.h>
#include "Lab2_FPThread.h"
#include "../../ENCM511_SpecificFiles/ENCM511_include/FrontPanel_LED_Switches.h"



//Aux. stubs
static bool Init_Switch_Interface_Done = false;
void My_Init_SwitchInterface(void){
	Init_GPIO_FrontPanelSwitches();
	//printf("Stub for Init_Switch_Interface\n");
	Init_Switch_Interface_Done = true;
}

static bool Init_LED_Interface_Done = false;
void My_Init_LED_Interface(void){
	Init_GPIO_FrontPanelLEDS();
	//printf("Stub for Init_LED_Interface\n");
	Init_LED_Interface_Done = true;
}


short int My_ReadSwitches(void){
	if (Init_Switch_Interface_Done) {
		//printf("Stub Pass: ReadSwitches\n");
		return Read_GPIO_FrontPanelSwitches();
	}
	printf("Stub Fail: ReadSwitches - Switch Hardware not initialized\n");
	return GARBAGE_VALUE;
}



void My_WriteLED(short int WriteLEDValue){

	if (Init_LED_Interface_Done) {
		unsigned char convertedWriteLEDValue;
		convertedWriteLEDValue = WriteLEDValue;
		Write_GPIO_FrontPanelLEDS(convertedWriteLEDValue);
		return; //Exit
}
	//Else
	printf("Stub Fail: WriteLED - LED Hardware not initialized\n");
}

short int My_ReadLED(void){
	if (Init_LED_Interface_Done) {
		//printf("Stub Pass: ReadLED\n");
		return Read_GPIO_FrontPanelLEDS(); //random variable
	}

	//Else
	printf("Stub Fail: ReadLED - LED Hardware not initialized\n");
	return GARBAGE_VALUE;
}



short int SwitchLower5BitMask(short int SwitchValueToMask){
	short int MaskLower5Bits = 0x001F;
	return ~SwitchValueToMask & MaskLower5Bits;
}

void WaitClkCycles(unsigned long int CountTimer) {
	unsigned long int StartCycle = ReadProcessorCyclesCPP();
	unsigned long int EndCycle = StartCycle + CountTimer;

	while (StartCycle < EndCycle) {
		StartCycle = ReadProcessorCyclesCPP();
	}
}
#if 0
void PrintNumberofREBCommands(unsigned short int * REB_ReadArray){
	//Check how many commands are in the REB array
	int NumberofREBCommands = END_OF_REB_READ_ARRAY;
	bool isZero = true;
	while (isZero){
		NumberofREBCommands = NumberofREBCommands-1;
		if (NumberofREBCommands < 0){ //we've exceeded the bounds of the array
			isZero = false;
			break;
		}
		isZero = REB_ReadArray[NumberofREBCommands] == 0x0 ? true : false;
	}
	NumberofREBCommands = NumberofREBCommands+1; //Number of commands by most human standards
	printf("Number of REB commands: %d \n", NumberofREBCommands);
	//End check number of commands
}
#endif

short int My_ReadSwitches_10ms_debounce(short int PreviousSwitchValue){
	short int FirstPress = SwitchLower5BitMask(My_ReadSwitches());
	WaitClkCycles(MILLISECOND_TEN);
	short int SecondPress = SwitchLower5BitMask(My_ReadSwitches());
	short int CurrentSwitchValue = (FirstPress == SecondPress) ? FirstPress : PreviousSwitchValue; //Bad decounce? just use previous switch value
	return CurrentSwitchValue;
}

void InitAll_REB_FP(){
	My_Init_SwitchInterface();
	My_Init_LED_Interface();
}
