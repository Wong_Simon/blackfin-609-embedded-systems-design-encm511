################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
ASM_SRCS += \
../uTTCOSg2017/Idle_WaitForInterrupts_ASM.asm \
../uTTCOSg2017/ReadCycles_ASM.asm 

SRC_OBJS += \
./uTTCOSg2017/Idle_WaitForInterrupts_ASM.doj \
./uTTCOSg2017/ReadCycles_ASM.doj 

ASM_DEPS += \
./uTTCOSg2017/Idle_WaitForInterrupts_ASM.d \
./uTTCOSg2017/ReadCycles_ASM.d 


# Each subdirectory must supply rules for building sources it contributes
uTTCOSg2017/Idle_WaitForInterrupts_ASM.doj: ../uTTCOSg2017/Idle_WaitForInterrupts_ASM.asm
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin Assembler'
	easmblkfn -file-attr ProjectName="Lab2_BF609_UTTCOS_Core0" -proc ADSP-BF609 -si-revision any -g -DCORE0 -D_DEBUG -DADI_MCAPI @includes-75cb79323463fd0ae821e5192386968b.txt -gnu-style-dependencies -MM -Mo "uTTCOSg2017/Idle_WaitForInterrupts_ASM.d" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

uTTCOSg2017/ReadCycles_ASM.doj: ../uTTCOSg2017/ReadCycles_ASM.asm
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin Assembler'
	easmblkfn -file-attr ProjectName="Lab2_BF609_UTTCOS_Core0" -proc ADSP-BF609 -si-revision any -g -DCORE0 -D_DEBUG -DADI_MCAPI @includes-75cb79323463fd0ae821e5192386968b.txt -gnu-style-dependencies -MM -Mo "uTTCOSg2017/ReadCycles_ASM.d" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


