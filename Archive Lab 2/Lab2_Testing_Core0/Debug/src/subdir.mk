################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
ASM_SRCS += \
../src/GeneralCode_Lab0n1_PORTF_ASM.asm \
../src/ReadProcessorCyclesASM.asm 

CPP_SRCS += \
../src/GeneralCode_Lab0n1.cpp \
../src/Lab2_BF609_UTTCOS_Core0.cpp \
../src/Lab2_FPThread.cpp \
../src/Lab2_Testing_Core0_EUNIT2017_main.cpp \
../src/ReadProcessorCyclesCPP.cpp 

SRC_OBJS += \
./src/GeneralCode_Lab0n1.doj \
./src/GeneralCode_Lab0n1_PORTF_ASM.doj \
./src/Lab2_BF609_UTTCOS_Core0.doj \
./src/Lab2_FPThread.doj \
./src/Lab2_Testing_Core0_EUNIT2017_main.doj \
./src/ReadProcessorCyclesASM.doj \
./src/ReadProcessorCyclesCPP.doj 

ASM_DEPS += \
./src/GeneralCode_Lab0n1_PORTF_ASM.d \
./src/ReadProcessorCyclesASM.d 

CPP_DEPS += \
./src/GeneralCode_Lab0n1.d \
./src/Lab2_BF609_UTTCOS_Core0.d \
./src/Lab2_FPThread.d \
./src/Lab2_Testing_Core0_EUNIT2017_main.d \
./src/ReadProcessorCyclesCPP.d 


# Each subdirectory must supply rules for building sources it contributes
src/GeneralCode_Lab0n1.doj: ../src/GeneralCode_Lab0n1.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn -c -file-attr ProjectName="Lab2_Testing_Core0" -proc ADSP-BF609 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG -DADI_MCAPI @includes-30881c9da67a53cdea7b3316b27b0029.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/GeneralCode_Lab0n1.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/GeneralCode_Lab0n1_PORTF_ASM.doj: ../src/GeneralCode_Lab0n1_PORTF_ASM.asm
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin Assembler'
	easmblkfn -file-attr ProjectName="Lab2_Testing_Core0" -proc ADSP-BF609 -si-revision any -g -DCORE0 -D_DEBUG -DADI_MCAPI @includes-5751acd4097d5122bd0f1fd5c00b2ac0.txt -gnu-style-dependencies -MM -Mo "src/GeneralCode_Lab0n1_PORTF_ASM.d" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/Lab2_BF609_UTTCOS_Core0.doj: ../src/Lab2_BF609_UTTCOS_Core0.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn -c -file-attr ProjectName="Lab2_Testing_Core0" -proc ADSP-BF609 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG -DADI_MCAPI @includes-30881c9da67a53cdea7b3316b27b0029.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/Lab2_BF609_UTTCOS_Core0.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/Lab2_FPThread.doj: ../src/Lab2_FPThread.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn -c -file-attr ProjectName="Lab2_Testing_Core0" -proc ADSP-BF609 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG -DADI_MCAPI @includes-30881c9da67a53cdea7b3316b27b0029.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/Lab2_FPThread.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/Lab2_Testing_Core0_EUNIT2017_main.doj: ../src/Lab2_Testing_Core0_EUNIT2017_main.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn -c -file-attr ProjectName="Lab2_Testing_Core0" -proc ADSP-BF609 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG -DADI_MCAPI @includes-30881c9da67a53cdea7b3316b27b0029.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/Lab2_Testing_Core0_EUNIT2017_main.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/ReadProcessorCyclesASM.doj: ../src/ReadProcessorCyclesASM.asm
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin Assembler'
	easmblkfn -file-attr ProjectName="Lab2_Testing_Core0" -proc ADSP-BF609 -si-revision any -g -DCORE0 -D_DEBUG -DADI_MCAPI @includes-5751acd4097d5122bd0f1fd5c00b2ac0.txt -gnu-style-dependencies -MM -Mo "src/ReadProcessorCyclesASM.d" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/ReadProcessorCyclesCPP.doj: ../src/ReadProcessorCyclesCPP.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn -c -file-attr ProjectName="Lab2_Testing_Core0" -proc ADSP-BF609 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG -DADI_MCAPI @includes-30881c9da67a53cdea7b3316b27b0029.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/ReadProcessorCyclesCPP.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


