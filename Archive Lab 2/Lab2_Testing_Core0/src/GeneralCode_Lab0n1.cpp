/*
 * 20190918_Lab0_GeneralCode.cpp
 *
 *  Created on: Sep 18, 2019
 *      Author: Simon Wong and Kevin Blakey
 */

//Headers
#include <sys/platform.h>
#include "Lab2_FPThread.h"
#include "../../ENCM511_SpecificFiles/ENCM511_include/FrontPanel_LED_Switches.h"


#if 0
//Primary stub
void Start_Lab(void) {

	//Initialization of hardware LAB0
	My_Init_SwitchInterface();
	My_Init_LED_Interface();

#if 1 //REB Stuff - LAB1

	//LAB 0 LOCALS
	unsigned long int EndTimer = SECOND_ONE/2; //Initialize refresh rate as 0.5 second
	//short int LED_Array[END_OF_LED_COMMANDS] = {0xff, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80}; //incrementing bit pattern, can be refactored to be a name
	short int PreviousSwitchValue = SWITCH_NONE; //Initialize
	short int LEDIndex = 0;

	//LAB 1 LOCALS
	unsigned short int REB_ReadArrayIndexReading = 0;
	unsigned short int REB_ReadArrayIndexWriting = 0;
	bool ToggleLab1Cycle = OFF;
	unsigned short int REB_ReadArray[END_OF_REB_READ_ARRAY] = {0xC00,0x300,0xF00,0x000,0x100,0x200,0x400,0x800}; //random bit pattern, can be refactored to a base value

#if 1 //My ASM inits
	My_Init_GPIO_REB_Input_ASM();
	My_Init_GPIO_REB_Output_ASM();
#endif //My ASM inits

#if 0 //Tests MS inits
	#include "../../ENCM511_SpecificFiles/ENCM511_include/REB_GPIO_Input_Library.h"
	#include "../../ENCM511_SpecificFiles/ENCM511_include/REB_GPIO_Output_Library.h"
	Init_GPIO_REB_Input();
	Init_GPIO_REB_Output();
#endif //Tests MS inits

#if 0 //Tests for ASM reading and writing
	printf("Cycle register: %i\n", ReadProcessorCyclesASM());
	printf("Read value of REBInputs: %i\n", My_Read_GPIO_REB_Input_ASM());
	printf("Writing to REBOutput all bits on.\n");
	My_Write_GPIO_REB_Output_ASM (0xF000);
#endif //Tests for ASM


	while (1) {
		//Write the next LED value - LAB0
		My_WriteLED(LED_Array[LEDIndex]);
		LEDIndex = (LEDIndex >= END_OF_LED_COMMANDS - 1) ? 0 : LEDIndex+1; //array increment or reset

		//Write the next REB instruction (toggleable)
		if (ToggleLab1Cycle/* = ON*/){
			My_Write_GPIO_REB_Output_ASM(REB_ReadArray[REB_ReadArrayIndexWriting] << 4);
			//printf("REB_ReadArray Write out: %x \n", REB_ReadArray[REB_ReadArrayIndexWriting]);
			REB_ReadArrayIndexWriting = (REB_ReadArrayIndexWriting >= END_OF_REB_READ_ARRAY - 1) ? 0 : REB_ReadArrayIndexWriting+1; //array increment or reset
		}

		//Read switch values
		short int CurrentSwitchValue = My_ReadSwitches_10ms_debounce(PreviousSwitchValue);

		//When a switch is released, find which bit it is (eg. falling edge bit pattern)
		short int SwitchValue = (~CurrentSwitchValue & PreviousSwitchValue);

		//Update previous switch value now that current value has been used.
		PreviousSwitchValue = CurrentSwitchValue;

		//What are we doing with the switch value?
		if ((SwitchValue & SWITCH_ONE)   == SWITCH_ONE  ) { //speed up refresh rate
			EndTimer = EndTimer*0.5;
		}


		if ((SwitchValue & SWITCH_TWO)   == SWITCH_TWO  ) { //slow down refresh rate
			EndTimer = (EndTimer >= 2*SECOND_ONE) ? 2*SECOND_ONE : EndTimer*2; //cap refresh rate at 2 seconds max.
		}


		if ((SwitchValue & SWITCH_THREE) == SWITCH_THREE) { //toggle autowriting of REB LEDs
			ToggleLab1Cycle = !ToggleLab1Cycle;
			//printf("ToggleLab1Cycle State: %d \n", ToggleLab1Cycle);

			//Reset array values to start of array.
			REB_ReadArrayIndexWriting = 0;
			REB_ReadArrayIndexReading = 0;
			PrintNumberofREBCommands(& REB_ReadArray[0]);
		}


		if (((SwitchValue & SWITCH_FOUR)  == SWITCH_FOUR) & !ToggleLab1Cycle ) { //increment REB LED, cant write when toggled on
			My_Write_GPIO_REB_Output_ASM(REB_ReadArray[REB_ReadArrayIndexWriting] << 4);
			//printf("REB_ReadArray Write out: %x \n", REB_ReadArray[REB_ReadArrayIndexWriting] << 4);
			REB_ReadArrayIndexWriting = (REB_ReadArrayIndexWriting >= END_OF_REB_READ_ARRAY - 1) ? 0 : REB_ReadArrayIndexWriting+1; //array increment or reset
		}


		if (((SwitchValue & SWITCH_FIVE)  == SWITCH_FIVE) & !ToggleLab1Cycle ) { //reading from REB switches, cant write when toggled on
			unsigned short int Read_REB_Input = My_Read_GPIO_REB_Input_ASM();

			//Check for illegals and junk if illegal.
			if ((ILLEGAL_MASK_FORWARDBACK == (Read_REB_Input & ILLEGAL_MASK_FORWARDBACK)) | (ILLEGAL_MASK_LEFTRIGHT == (Read_REB_Input & ILLEGAL_MASK_LEFTRIGHT))){
				printf("REB_Input value read in is illegal. That value will not be stored.\n");
			}
			else {
				REB_ReadArray[REB_ReadArrayIndexReading] = My_Read_GPIO_REB_Input_ASM();
				//printf("REB_ReadArray Read in: %x \n", REB_ReadArray[REB_ReadArrayIndexReading]);
				REB_ReadArrayIndexReading = (REB_ReadArrayIndexReading >= END_OF_REB_READ_ARRAY - 1) ? 0 : REB_ReadArrayIndexReading+1; //array increment or reset
			}
		}


		//Wait until the clock is completed
		while (ReadProcessorCyclesASM() < EndTimer){}
		ResetProcessorCyclesToZeroASM();
	}

#endif //End lab 1

}

#endif


//Aux. stubs
static bool Init_Switch_Interface_Done = false;
void My_Init_SwitchInterface(void){
	Init_GPIO_FrontPanelSwitches();
	//printf("Stub for Init_Switch_Interface\n");
	Init_Switch_Interface_Done = true;
}

static bool Init_LED_Interface_Done = false;
void My_Init_LED_Interface(void){
	Init_GPIO_FrontPanelLEDS();
	//printf("Stub for Init_LED_Interface\n");
	Init_LED_Interface_Done = true;
}


short int My_ReadSwitches(void){
	if (Init_Switch_Interface_Done) {
		//printf("Stub Pass: ReadSwitches\n");
		return Read_GPIO_FrontPanelSwitches();
	}
	printf("Stub Fail: ReadSwitches - Switch Hardware not initialized\n");
	return GARBAGE_VALUE;
}



void My_WriteLED(short int WriteLEDValue){

	if (Init_LED_Interface_Done) {
		unsigned char convertedWriteLEDValue;
		convertedWriteLEDValue = WriteLEDValue;
		Write_GPIO_FrontPanelLEDS(convertedWriteLEDValue);
		return; //Exit
}
	//Else
	printf("Stub Fail: WriteLED - LED Hardware not initialized\n");
}

short int My_ReadLED(void){
	if (Init_LED_Interface_Done) {
		//printf("Stub Pass: ReadLED\n");
		return Read_GPIO_FrontPanelLEDS(); //random variable
	}

	//Else
	printf("Stub Fail: ReadLED - LED Hardware not initialized\n");
	return GARBAGE_VALUE;
}



short int SwitchLower5BitMask(short int SwitchValueToMask){
	short int MaskLower5Bits = 0x001F;
	return ~SwitchValueToMask & MaskLower5Bits;
}

void WaitClkCycles(unsigned long int CountTimer) {
	unsigned long int StartCycle = ReadProcessorCyclesCPP();
	unsigned long int EndCycle = StartCycle + CountTimer;

	while (StartCycle < EndCycle) {
		StartCycle = ReadProcessorCyclesCPP();
	}
}
#if 0
void PrintNumberofREBCommands(unsigned short int * REB_ReadArray){
	//Check how many commands are in the REB array
	int NumberofREBCommands = END_OF_REB_READ_ARRAY;
	bool isZero = true;
	while (isZero){
		NumberofREBCommands = NumberofREBCommands-1;
		if (NumberofREBCommands < 0){ //we've exceeded the bounds of the array
			isZero = false;
			break;
		}
		isZero = REB_ReadArray[NumberofREBCommands] == 0x0 ? true : false;
	}
	NumberofREBCommands = NumberofREBCommands+1; //Number of commands by most human standards
	printf("Number of REB commands: %d \n", NumberofREBCommands);
	//End check number of commands
}
#endif

short int My_ReadSwitches_10ms_debounce(short int PreviousSwitchValue){
	short int FirstPress = SwitchLower5BitMask(My_ReadSwitches());
	WaitClkCycles(MILLISECOND_TEN);
	short int SecondPress = SwitchLower5BitMask(My_ReadSwitches());
	short int CurrentSwitchValue = (FirstPress == SecondPress) ? FirstPress : PreviousSwitchValue; //Bad decounce? just use previous switch value
	return CurrentSwitchValue;
}

void InitAll_REB_FP(){
	My_Init_SwitchInterface();
	My_Init_LED_Interface();
	My_Init_GPIO_REB_Input_ASM();
	My_Init_GPIO_REB_Output_ASM();
}
