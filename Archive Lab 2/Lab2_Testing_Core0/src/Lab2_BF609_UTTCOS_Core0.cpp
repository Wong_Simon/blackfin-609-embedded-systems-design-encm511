/*************************************************************************************
 *   AUTOMATICALLY GENERATED COMMENT -- DO NOT MODIFY
 * Author: Simon
 * Date: Thu 2019/11/21 at 07:51:41 PM
 * File Type: EUNIT Test File
 *************************************************************************************/

#define EMBEDDEDUNIT_LITE
#include <EmbeddedUnit2017/EmbeddedUnit2017.h>
#include "Lab2_BF609_UTTCOS_Core0.h"
#include "Lab2_FPThread.h"
#include "GeneralCode_Lab0n1.h"

TEST_CONTROL(Lab2_BF609_UTTCOS_Core0_cpp);

#if 1
void UpdateEunitGui(void);
TEST(Lab2_BF609_UTTCOS_Core0_cpp_GUIUpdate) {
	UpdateEunitGui(); // Conditionally compile this line (use #if 0) to stop an GUI update based on last completed test
}
#endif

unsigned short int TestBitwiseAND(unsigned short int bitPattern,
		unsigned short int bitMask);
unsigned short int TestBitwiseOR(unsigned short int bitPattern,
		unsigned short int bitMask);

#if 0
TEST(Threads1to3_Test_MoreComplexTest)
{
#warning 'Dummy test has been inserted -- replace with your own -- Thu 2019/11/21 at 07:51:41 PM '
	// TODO -- 'Dummy test has been inserted  -- replace with your own -- Thu 2019/11/21 at 07:51:41 PM '
	printf("Dummy test has been inserted -- replace with your own -- Thu 2019/11/21 at 07:51:41 PM \n");

	unsigned long int value = 0x01FF01FF;
	unsigned long int ORmask = 0x0F000F0F;
	unsigned long int expectedORResult = 0x0100010F;
	unsigned long int resultOR = TestBitwiseOR(value, ORmask);
	CHECK(expectedORResult == resultOR);
	CHECK_EQUAL(expectedORResult, resultOR);

#error("You insert the 'wrong' test for TestBitwiseAND";
}

unsigned short int TestBitwiseAND(unsigned short int bitPattern, unsigned short int bitMask) {
	return bitPattern && bitMask;
}

unsigned short int TestBitwiseOR(unsigned short int bitPattern, unsigned short int bitMask) {
	return bitPattern || bitMask;
}

TEST(Threads1to3_Test_Successes)
{
#warning 'Dummy test has been inserted -- replace with your own -- Thu 2019/11/21 at 07:51:41 PM '
	// TODO -- 'Dummy test has been inserted  -- replace with your own -- Thu 2019/11/21 at 07:51:41 PM '
	printf("Dummy test has been inserted -- replace with your own -- Thu 2019/11/21 at 07:51:41 PM \n");

	CHECK(false == false);
	CHECK_EQUAL(false, false);

	XF_CHECK(false == true);// Expected failure occurs
	XF_CHECK_EQUAL(false, true);// Expected failure occurs
	XF_CHECK(false == false);// Expected failure does not occur
	XF_CHECK_EQUAL(false, false);// Expected failure does not occur

#error("You insert the 'wrong' test for TestBitwiseAND";
}

#endif

TEST(FP_Thread1) {
	My_Init_All();
	My_WriteLED(0); //Init all LEDs to off
	for (int i = 0; i < 100; i++) {
		static short int checkValue = 0x80;
		PWM_FP_LED8();
		CHECK(My_ReadLED() == checkValue);
		if (checkValue == 0x80)
			checkValue = 0;
		else
			checkValue = 0x80;
	}
}

TEST(FP_Thread2) {
	My_Init_All();
	My_WriteLED(0); //Init all LEDs to off
	for (int i = 0; i < 99; i++) {
		static short int checkValue = 0x40;
		static short int stateVariable = 0;
		PWM_FP_LED7();
		if (stateVariable == 0) {
			checkValue = 0x40;
			stateVariable++;
		} else if (stateVariable == 1) {
			checkValue = 0x40;
			stateVariable++;
		} else {
			checkValue = 0;
			stateVariable = 0;
		}
		CHECK(My_ReadLED() == checkValue);
	}
}

TEST(FP_Thread3) {
	My_Init_All();
	My_WriteLED(0); //Init all LEDs to off
	for (int i = 0; i < 100; i++) {
		static short int checkValue[4] = { 0x0, 0x1, 0x2, 0x3 };
		static short int index = 0;
		CLK_COUNTER_LED1_2();
		CHECK(My_ReadLED() == checkValue[index]);
		if (index >= 3)
			index = 0;
		else
			index++;
	}
}

TEST(FP_ThreadAll) {
	My_Init_All();
	My_WriteLED(0); //Init all LEDs to off
	for (int i = 0; i < 101; i++) {
		static short int checkValue1 = 0x80;
		static short int checkValue2[3] = { 0x40, 0x40, 0x0 };
		static short int stateVariable = 0;
		static short int checkValue[4] = { 0x0, 0x1, 0x2, 0x3 };
		static short int index = 0;

		PWM_FP_LED7();
		PWM_FP_LED8();
		CLK_COUNTER_LED1_2();

		CHECK((My_ReadLED() & FP_LED_8) == checkValue1);
		if (checkValue1 == 0x80)
			checkValue1 = 0;
		else
			checkValue1 = 0x80;

		CHECK((My_ReadLED() & FP_LED_7) == checkValue2[stateVariable]);
		if (stateVariable >= 2)
			stateVariable = 0;
		else
			stateVariable++;

		CHECK((My_ReadLED() & (FP_LED_1 | FP_LED_2)) == checkValue[index]);
		if (index >= 3)
			index = 0;
		else
			index++;

	}
}

TEST_FILE_RUN_NOTIFICATION(Lab2_BF609_UTTCOS_Core0_cpp);

