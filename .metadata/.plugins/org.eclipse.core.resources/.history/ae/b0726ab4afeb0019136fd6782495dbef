/*
 * 20190918_Lab0_GeneralCode.cpp
 *
 *  Created on: Sep 18, 2019
 *      Author: simon
 */

//Defined values
#define GARBAGE_VALUE -1

//Headers
#include <sys/platform.h>
#include "adi_initialize.h"
#include "20190918_BF609_Lab0_Core0.h"
#include "../../ENCM511_SpecificFiles/ENCM511_include/FrontPanel_LED_Switches.h"

//Prototypes
void My_Init_SwitchInterface(void);
void My_Init_LED_Interface(void);

short int My_ReadSwitches(void);
void My_WriteLED(short int WriteLEDValue);
short int My_ReadLED(void);

void My_PrintBinaryValue(short int IntToBeConverted);
short int SwitchLower5BitMask(short int SwitchValueToMask);
void WaitClkCycles(unsigned long int CountTimer);


//Private global variable
static bool Init_Switch_Interface_Done = false;
static bool Init_LED_Interface_Done = false;
static short int LastLEDValueWritten = GARBAGE_VALUE;

//Primary stub
void Start_Lab0(void) {

//Defined values used for LAB0
#define SECOND_ONE 500000000
#define MILLISECOND_ONEHUNDRED 50000000
#define MILLISECOND_TEN 5000000
#define END_OF_LED_COMMANDS 9
#define END_OF_REB_READ_ARRAY 8
#define SWITCH_NONE 0x00
#define SWITCH_ONE 0x01
#define SWITCH_TWO 0x02
#define SWITCH_THREE 0x04
#define SWITCH_FOUR 0x08
#define SWITCH_FIVE 0x10

	//Initialization of hardware
	My_Init_SwitchInterface();
	My_Init_LED_Interface();

#if 1 //REB Stuff - LAB1
	Init_GPIO_REB_Input();
	Init_GPIO_REB_Output();

	//LAB 0 LOCALS
	unsigned long int EndTimer = SECOND_ONE/2; //Initialize refresh rate as 0.5 second

	short int LED_Array[END_OF_LED_COMMANDS] = {0xff, 0x01, 0x02, 0x04, 0x08,
			0x10, 0x20, 0x40, 0x80};
	short int PreviousSwitchValue = SWITCH_NONE; //Initialize
	short int LEDIndex = 0;

	//LAB 1 LOCALS
	unsigned short int REB_ReadArray[END_OF_REB_READ_ARRAY] = {0,0,0,0,0,0,0,0};
	unsigned short int REB_ReadArrayIndexReading = 0;
	unsigned short int REB_ReadArrayIndexWriting = 0;

	while (1) {
			//Write the next LED value
			if (LEDIndex == END_OF_LED_COMMANDS) { LEDIndex = 0; } //Reset index if its gone off
			My_WriteLED(LED_Array[LEDIndex]);
			LEDIndex += 1;

			//Write the REB LED
			//Write_GPIO_REB_Output(REB_ReadArray[REB_ReadArrayIndexWriting]);

			//Read switches with 10ms debounce
			short int FirstPress = SwitchLower5BitMask(My_ReadSwitches());
			WaitClkCycles(MILLISECOND_TEN);
			short int SecondPress = SwitchLower5BitMask(My_ReadSwitches());
			short int CurrentSwitchValue = (FirstPress == SecondPress) ? FirstPress : PreviousSwitchValue; //Bad decounce? just use previous switch value

			//When a switch is released, find which bit it is (eg. falling edge bit pattern)
			short int SwitchValue = (~CurrentSwitchValue & PreviousSwitchValue);

			//What are we doing with the switch value?
			if ((SwitchValue & SWITCH_ONE)   == SWITCH_ONE  ) { //speed up refresh rate
				EndTimer = EndTimer*0.5;
			}
			if ((SwitchValue & SWITCH_TWO)   == SWITCH_TWO  ) { //slow down refresh rate
				EndTimer = (EndTimer >= 2*SECOND_ONE) ? 2*SECOND_ONE : EndTimer*2; //cap refresh rate at 2 seconds max.
			}
			if ((SwitchValue & SWITCH_THREE) == SWITCH_THREE) {}
			if ((SwitchValue & SWITCH_FOUR)  == SWITCH_FOUR ) { //writing to REB LED
				Write_GPIO_REB_Output(REB_ReadArray[REB_ReadArrayIndexWriting] << 4);
				printf("REB_ReadArray Write out: %x \n", REB_ReadArray[REB_ReadArrayIndexWriting]);
				REB_ReadArrayIndexWriting = (REB_ReadArrayIndexWriting >= END_OF_REB_READ_ARRAY) ? 0 : REB_ReadArrayIndexWriting+1; //array increment or reset
			}
			if ((SwitchValue & SWITCH_FIVE)  == SWITCH_FIVE ) { //reading from RED switches
				REB_ReadArray[REB_ReadArrayIndexReading] = Read_GPIO_REB_Input();
				printf("REB_ReadArray Read in: %x \n", REB_ReadArray[REB_ReadArrayIndexReading]);
				REB_ReadArrayIndexReading = (REB_ReadArrayIndexReading >= END_OF_REB_READ_ARRAY) ? 0 : REB_ReadArrayIndexReading+1; //array increment or reset

			}

			//Update previous now that current value has been used.
			PreviousSwitchValue = CurrentSwitchValue;

			//Wait until the clock is completed
			while (ReadProcessorCyclesCPP() < EndTimer){}
			ResetProcessorCyclestoZeroCPP();
		}

#endif

#if 0 //Time activated super-loop - LAB0

	unsigned long int EndTimer = SECOND_ONE/2; //Initialize refresh rate as 0.5 second

	short int LED_Array[END_OF_LED_COMMANDS] = {0xff, 0x01, 0x02, 0x04, 0x08,
			0x10, 0x20, 0x40, 0x80};
	short int PreviousSwitchValue = SWITCH_NONE; //Initialize
	short int LEDIndex = 0;
	while (1) {
		//Write the next LED value
		if (LEDIndex == END_OF_LED_COMMANDS) { LEDIndex = 0; } //Reset index if its gone off
		My_WriteLED(LED_Array[LEDIndex]);
		LEDIndex += 1;

		//Read switches with 10ms debounce
		short int FirstPress = SwitchLower5BitMask(My_ReadSwitches());
		WaitClkCycles(MILLISECOND_TEN);
		short int SecondPress = SwitchLower5BitMask(My_ReadSwitches());
		short int CurrentSwitchValue = (FirstPress == SecondPress) ? FirstPress : PreviousSwitchValue; //Bad decounce? just use previous switch value

		//When a switch is released, find which bit it is (eg. falling edge bit pattern)
		short int SwitchValue = (~CurrentSwitchValue & PreviousSwitchValue);

		//What are we doing with the switch value?
		if ((SwitchValue & SWITCH_ONE)   == SWITCH_ONE  ) { //speed up refresh rate
			EndTimer = EndTimer*0.5;
		}
		if ((SwitchValue & SWITCH_TWO)   == SWITCH_TWO  ) { //slow down refresh rate
			EndTimer = (EndTimer >= 2*SECOND_ONE) ? 2*SECOND_ONE : EndTimer*2; //cap refresh rate at 2 seconds max.
		}
		if ((SwitchValue & SWITCH_THREE) == SWITCH_THREE) {}
		if ((SwitchValue & SWITCH_FOUR)  == SWITCH_FOUR ) {}
		if ((SwitchValue & SWITCH_FIVE)  == SWITCH_FIVE ) {}

		//Update previous now that current value has been used.
		PreviousSwitchValue = CurrentSwitchValue;

		//Wait until the clock is completed
		while (ReadProcessorCyclesCPP() < EndTimer){}
		ResetProcessorCyclestoZeroCPP();
	}

#endif

	printf("Exiting Start_Lab0\n");
}


//Aux. stubs
void My_Init_SwitchInterface(void){
	Init_GPIO_FrontPanelSwitches();
	//printf("Stub for Init_Switch_Interface\n");
	Init_Switch_Interface_Done = true;
}


void My_Init_LED_Interface(void){
	Init_GPIO_FrontPanelLEDS();
	//printf("Stub for Init_LED_Interface\n");
	Init_LED_Interface_Done = true;
}


short int My_ReadSwitches(void){
	if (Init_Switch_Interface_Done) {
		//printf("Stub Pass: ReadSwitches\n");
		return Read_GPIO_FrontPanelSwitches();
	}
	printf("Stub Fail: ReadSwitches - Switch Hardware not initialized\n");
	return GARBAGE_VALUE;
}


void My_WriteLED(short int WriteLEDValue){
#if 0 //Old code for printing to console
	if (Init_LED_Interface_Done) {
		//printf("Stub Pass: WriteLED\n");
		My_PrintBinaryValue(WriteLEDValue);
		LastLEDValueWritten = WriteLEDValue;
		return; //Exit
	}
#endif

#if 1
	if (Init_LED_Interface_Done) {
		unsigned char convertedWriteLEDValue;
		convertedWriteLEDValue = WriteLEDValue;
		Write_GPIO_FrontPanelLEDS(convertedWriteLEDValue);
		return; //Exit
}
#endif
	//Else
	printf("Stub Fail: WriteLED - LED Hardware not initialized\n");
}


short int My_ReadLED(void){
	if (Init_LED_Interface_Done) {
		printf("Stub Pass: ReadLED\n");
		return LastLEDValueWritten; //random variable
	}

	//Else
	printf("Stub Fail: ReadLED - LED Hardware not initialized\n");
	return GARBAGE_VALUE;
}

void My_PrintBinaryValue(short int IntToBeConverted){
	//printf("Stub for PrintLEDValue\n");
	char BitPattern[17] = {' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ', 0x0};
	unsigned int TempBitPatternIndex = (sizeof(BitPattern)/sizeof(BitPattern[0])) - 2; //Get last index of BitPattern

	int TempIntValue= IntToBeConverted;
	while(TempIntValue != 0) {
		BitPattern[TempBitPatternIndex] = (TempIntValue%2 != 0) ? '1' : ' ';
		TempBitPatternIndex = TempBitPatternIndex - 1;
		TempIntValue = TempIntValue/2;
	}

	printf("PBVfI - Decimal %3i - Hex 0x%2X - Binary %s\n", IntToBeConverted, IntToBeConverted, BitPattern);
}

short int SwitchLower5BitMask(short int SwitchValueToMask){
	short int MaskLower5Bits = 0x001F;
	return ~SwitchValueToMask & MaskLower5Bits;
}

void WaitClkCycles(unsigned long int CountTimer) {
	unsigned long int StartCycle = ReadProcessorCyclesCPP();
	unsigned long int EndCycle = StartCycle + CountTimer;

	while (StartCycle < EndCycle) {
		StartCycle = ReadProcessorCyclesCPP();
	}
}
