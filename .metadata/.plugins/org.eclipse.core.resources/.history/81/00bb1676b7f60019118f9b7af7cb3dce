/* init the output of data register in asm
 * move defines and prototypes / ask ta where they want stubs
 * switch actions can be stubbed
 * debounce can be stubbed
 *
 * 20190918_Lab0_GeneralCode.cpp
 *
 *  Created on: Sep 18, 2019
 *      Author: simon
 */

//Defined values
#define GARBAGE_VALUE -1


//Defined values used for LAB0
#define SECOND_ONE 500000000
#define MILLISECOND_ONEHUNDRED 50000000
#define MILLISECOND_TEN 5000000
#define END_OF_LED_COMMANDS 9
#define END_OF_REB_READ_ARRAY 8
#define SWITCH_NONE 0x00
#define SWITCH_ONE 0x01
#define SWITCH_TWO 0x02
#define SWITCH_THREE 0x04
#define SWITCH_FOUR 0x08
#define SWITCH_FIVE 0x10
#define ON true
#define OFF false
#define ILLEGAL_MASK_LEFTRIGHT 0x500
#define ILLEGAL_MASK_FORWARDBACK 0xa00


//Headers
#include <sys/platform.h>
#include "20190918_BF609_Lab0_Core0.h"
#include "../../ENCM511_SpecificFiles/ENCM511_include/FrontPanel_LED_Switches.h"

//Prototypes
void My_Init_SwitchInterface(void);
void My_Init_LED_Interface(void);

short int My_ReadSwitches(void);
void My_WriteLED(short int WriteLEDValue);
short int My_ReadLED(void);

void My_PrintBinaryValue(short int IntToBeConverted);
short int SwitchLower5BitMask(short int SwitchValueToMask);
void WaitClkCycles(unsigned long int CountTimer);


//Global variable
static bool Init_Switch_Interface_Done = false;
static bool Init_LED_Interface_Done = false;
static short int LastLEDValueWritten = GARBAGE_VALUE;


//Primary stub
void Start_Lab0(void) {

	//Initialization of hardware
	My_Init_SwitchInterface();
	My_Init_LED_Interface();

#if 1 //REB Stuff - LAB1
	My_Init_GPIO_REB_Input_ASM();
	My_Init_GPIO_REB_Output_ASM();

	//LAB 0 LOCALS
	unsigned long int EndTimer = SECOND_ONE/2; //Initialize refresh rate as 0.5 second

	short int LED_Array[END_OF_LED_COMMANDS] = {0xff, 0x01, 0x02, 0x04, 0x08,
			0x10, 0x20, 0x40, 0x80};
	short int PreviousSwitchValue = SWITCH_NONE; //Initialize
	short int LEDIndex = 0;

	//LAB 1 LOCALS
	unsigned short int REB_ReadArray[END_OF_REB_READ_ARRAY] = {0xC00,0x300,0xF00,0x000,0x100,0x200,0x400,0x800};
	unsigned short int REB_ReadArrayIndexReading = 0;
	unsigned short int REB_ReadArrayIndexWriting = 0;
	bool ToggleLab1Cycle = OFF;


	while (1) {
		//Read switches with 10ms debounce
		short int FirstPress = SwitchLower5BitMask(My_ReadSwitches());
		WaitClkCycles(MILLISECOND_TEN);
		short int SecondPress = SwitchLower5BitMask(My_ReadSwitches());
		short int CurrentSwitchValue = (FirstPress == SecondPress) ? FirstPress : PreviousSwitchValue; //Bad decounce? just use previous switch value

		//When a switch is released, find which bit it is (eg. falling edge bit pattern)
		short int SwitchValue = (~CurrentSwitchValue & PreviousSwitchValue);

		//Write the next LED value - LAB0
		My_WriteLED(LED_Array[LEDIndex]);
		LEDIndex = (LEDIndex >= END_OF_LED_COMMANDS - 1) ? 0 : LEDIndex+1; //array increment or reset

		//Write the next REB instruction (toggleable)
		if (ToggleLab1Cycle/* = ON*/){
			My_Write_GPIO_REB_Output_ASM(REB_ReadArray[REB_ReadArrayIndexWriting] << 4);
			//printf("REB_ReadArray Write out: %x \n", REB_ReadArray[REB_ReadArrayIndexWriting]);
			REB_ReadArrayIndexWriting = (REB_ReadArrayIndexWriting >= END_OF_REB_READ_ARRAY - 1) ? 0 : REB_ReadArrayIndexWriting+1; //array increment or reset
		}

		//What are we doing with the switch value?
		if ((SwitchValue & SWITCH_ONE)   == SWITCH_ONE  ) { //speed up refresh rate
			EndTimer = EndTimer*0.5;
		}
		if ((SwitchValue & SWITCH_TWO)   == SWITCH_TWO  ) { //slow down refresh rate
			EndTimer = (EndTimer >= 2*SECOND_ONE) ? 2*SECOND_ONE : EndTimer*2; //cap refresh rate at 2 seconds max.
		}
		if ((SwitchValue & SWITCH_THREE) == SWITCH_THREE) { //toggle autowriting of REB LEDs
			ToggleLab1Cycle = !ToggleLab1Cycle;
			//printf("ToggleLab1Cycle State: %d \n", ToggleLab1Cycle);

			//Reset array values to start of array.
			REB_ReadArrayIndexWriting = 0;
			REB_ReadArrayIndexReading = 0;

#if 1
			//Check how many commands are in the REB array
			int NumberofREBCommands = END_OF_REB_READ_ARRAY;
			bool isZero = true;
			while (isZero){
				NumberofREBCommands = NumberofREBCommands-1;
				isZero = REB_ReadArray[NumberofREBCommands] = 0x0 ? true : false;
				if (NumberofREBCommands < 0){
					isZero = false;
				}
			}
			NumberofREBCommands = NumberofREBCommands+1; //Number of commands by most human standards
			printf("Number of REB commands: %d \n", NumberofREBCommands);
#endif
		}
		if (((SwitchValue & SWITCH_FOUR)  == SWITCH_FOUR) & !ToggleLab1Cycle ) { //increment REB LED, cant write when toggled on
			My_Write_GPIO_REB_Output_ASM(REB_ReadArray[REB_ReadArrayIndexWriting] << 4);
			//printf("REB_ReadArray Write out: %x \n", REB_ReadArray[REB_ReadArrayIndexWriting] << 4);
			REB_ReadArrayIndexWriting = (REB_ReadArrayIndexWriting >= END_OF_REB_READ_ARRAY - 1) ? 0 : REB_ReadArrayIndexWriting+1; //array increment or reset
		}
		if (((SwitchValue & SWITCH_FIVE)  == SWITCH_FIVE) & !ToggleLab1Cycle ) { //reading from REB switches, cant write when toggled on
			unsigned short int Read_REB_Input = My_Read_GPIO_REB_Input_ASM();
			if ((ILLEGAL_MASK_FORWARDBACK == (Read_REB_Input & ILLEGAL_MASK_FORWARDBACK)) | (ILLEGAL_MASK_LEFTRIGHT == (Read_REB_Input & ILLEGAL_MASK_LEFTRIGHT))){
				printf("REB_Input value read in is illegal. That value will not be stored.\n");
			}
			else {
				REB_ReadArray[REB_ReadArrayIndexReading] = My_Read_GPIO_REB_Input_ASM();
				//printf("REB_ReadArray Read in: %x \n", REB_ReadArray[REB_ReadArrayIndexReading]);
				REB_ReadArrayIndexReading = (REB_ReadArrayIndexReading >= END_OF_REB_READ_ARRAY - 1) ? 0 : REB_ReadArrayIndexReading+1; //array increment or reset
			}
		}

			//Update previous now that current value has been used.
			PreviousSwitchValue = CurrentSwitchValue;

			//Wait until the clock is completed
			while (ReadProcessorCyclesASM() < EndTimer){}
			ResetProcessorCyclesToZeroASM();
	}

#endif


	//printf("Exiting Start_Lab0\n");
}


//Aux. stubs
void My_Init_SwitchInterface(void){
	Init_GPIO_FrontPanelSwitches();
	//printf("Stub for Init_Switch_Interface\n");
	Init_Switch_Interface_Done = true;
}


void My_Init_LED_Interface(void){
	Init_GPIO_FrontPanelLEDS();
	//printf("Stub for Init_LED_Interface\n");
	Init_LED_Interface_Done = true;
}


short int My_ReadSwitches(void){
	if (Init_Switch_Interface_Done) {
		//printf("Stub Pass: ReadSwitches\n");
		return Read_GPIO_FrontPanelSwitches();
	}
	printf("Stub Fail: ReadSwitches - Switch Hardware not initialized\n");
	return GARBAGE_VALUE;
}


void My_WriteLED(short int WriteLEDValue){
#if 0 //Old code for printing to console
	if (Init_LED_Interface_Done) {
		//printf("Stub Pass: WriteLED\n");
		My_PrintBinaryValue(WriteLEDValue);
		LastLEDValueWritten = WriteLEDValue;
		return; //Exit
	}
#endif

#if 1
	if (Init_LED_Interface_Done) {
		unsigned char convertedWriteLEDValue;
		convertedWriteLEDValue = WriteLEDValue;
		Write_GPIO_FrontPanelLEDS(convertedWriteLEDValue);
		return; //Exit
}
#endif
	//Else
	printf("Stub Fail: WriteLED - LED Hardware not initialized\n");
}


short int My_ReadLED(void){
	if (Init_LED_Interface_Done) {
		printf("Stub Pass: ReadLED\n");
		return LastLEDValueWritten; //random variable
	}

	//Else
	printf("Stub Fail: ReadLED - LED Hardware not initialized\n");
	return GARBAGE_VALUE;
}

void My_PrintBinaryValue(short int IntToBeConverted){
	//printf("Stub for PrintLEDValue\n");
	char BitPattern[17] = {' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ', 0x0};
	unsigned int TempBitPatternIndex = (sizeof(BitPattern)/sizeof(BitPattern[0])) - 2; //Get last index of BitPattern

	int TempIntValue= IntToBeConverted;
	while(TempIntValue != 0) {
		BitPattern[TempBitPatternIndex] = (TempIntValue%2 != 0) ? '1' : ' ';
		TempBitPatternIndex = TempBitPatternIndex - 1;
		TempIntValue = TempIntValue/2;
	}

	printf("PBVfI - Decimal %3i - Hex 0x%2X - Binary %s\n", IntToBeConverted, IntToBeConverted, BitPattern);
}

short int SwitchLower5BitMask(short int SwitchValueToMask){
	short int MaskLower5Bits = 0x001F;
	return ~SwitchValueToMask & MaskLower5Bits;
}

void WaitClkCycles(unsigned long int CountTimer) {
	unsigned long int StartCycle = ReadProcessorCyclesCPP();
	unsigned long int EndCycle = StartCycle + CountTimer;

	while (StartCycle < EndCycle) {
		StartCycle = ReadProcessorCyclesCPP();
	}
}
