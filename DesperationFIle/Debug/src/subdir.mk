################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
ASM_SRCS += \
../src/ReadCycles_ASM.asm 

CPP_SRCS += \
../src/CoreTimer_Functions.cpp \
../src/DesperationFIle_EUNIT2017_main.cpp \
../src/DesperationTest.cpp 

SRC_OBJS += \
./src/CoreTimer_Functions.doj \
./src/DesperationFIle_EUNIT2017_main.doj \
./src/DesperationTest.doj \
./src/ReadCycles_ASM.doj 

ASM_DEPS += \
./src/ReadCycles_ASM.d 

CPP_DEPS += \
./src/CoreTimer_Functions.d \
./src/DesperationFIle_EUNIT2017_main.d \
./src/DesperationTest.d 


# Each subdirectory must supply rules for building sources it contributes
src/CoreTimer_Functions.doj: ../src/CoreTimer_Functions.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn -c -file-attr ProjectName="DesperationFIle" -proc ADSP-BF533 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG @includes-d28b10bc19854c85c08f9482ce6e7f2a.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/CoreTimer_Functions.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/DesperationFIle_EUNIT2017_main.doj: ../src/DesperationFIle_EUNIT2017_main.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn -c -file-attr ProjectName="DesperationFIle" -proc ADSP-BF533 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG @includes-d28b10bc19854c85c08f9482ce6e7f2a.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/DesperationFIle_EUNIT2017_main.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/DesperationTest.doj: ../src/DesperationTest.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn -c -file-attr ProjectName="DesperationFIle" -proc ADSP-BF533 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG @includes-d28b10bc19854c85c08f9482ce6e7f2a.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/DesperationTest.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/ReadCycles_ASM.doj: ../src/ReadCycles_ASM.asm
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin Assembler'
	easmblkfn -file-attr ProjectName="DesperationFIle" -proc ADSP-BF533 -si-revision any -g -DCORE0 -D_DEBUG @includes-186b656d93628358e077055f05b7affb.txt -gnu-style-dependencies -MM -Mo "src/ReadCycles_ASM.d" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


