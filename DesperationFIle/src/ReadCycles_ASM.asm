/*
 * ReadCycles_ASM.asm
 *
 *  Created on: Nov 22, 2019
 *      Author: Simon
 */

 .section program;
 .global _ReadCycles_ASM;
 
 _ReadCycles_ASM:
 	R0 = cycles;
 	R1 = cycles2;
 	nop;
 	nop;
 	nop;
 	nop;
 _ReadCycles_ASM.END:
 	RTS;