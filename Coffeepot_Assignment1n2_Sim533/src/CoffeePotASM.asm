.section program;
.global _InitLED_TestLED_ASM;

//#include <Coffeepot_Assignment1n2_Sim533.h>	//This failed because of the structure, but I don't need much of it

#define LED_DISPLAY_ENABLE_BIT 0x0002			//This is all I needed from it
#define MaskREMOVE_CURRENT_LED_VALUES 0x0FFF

#define ptrCoffeePotXParam_R0 R0
#define ptrCoffeePotX_P0 P0
#define ptrCoffeePotXControlRegister_P0 P0
#define tempControlRegister_R0 R0
#define tempMaskLED_DISPLAY_ENABLE_BIT_R1 R1
#define tempMaskREMOVE_CURRENT_LED_VALUES_R2 R2
#define LEDTestPattern_R1 R1
#define LEDTestPatternMax_R2 R2
#define PassParameterToFF_R0 R0

.extern __Z28FastForward_OneSimulationTICP16COFFEEPOT_DEVICE; //This name mangle took me 2 hours to find

_InitLED_TestLED_ASM:
LINK 20;

	//CoffeePot_X->controlRegister = CoffeePot_X->controlRegister | LED_DISPLAY_ENABLE_BIT;
	ptrCoffeePotX_P0 = ptrCoffeePotXParam_R0; //P0 has the address for the controlRegister since offset is 0
	tempControlRegister_R0 = W[ptrCoffeePotXControlRegister_P0](Z);
	tempMaskLED_DISPLAY_ENABLE_BIT_R1 = LED_DISPLAY_ENABLE_BIT(Z);
	tempControlRegister_R0 = tempControlRegister_R0 | tempMaskLED_DISPLAY_ENABLE_BIT_R1;
	W[ptrCoffeePotXControlRegister_P0] = tempControlRegister_R0;
	
	LEDTestPattern_R1 = 0x1000(Z); //	unsigned short int LEDTestPattern = 0x1000;
	
	//while(1){
	StartForeverLoop:
	
		//	if (LEDTestPattern < 0x8000)
		LEDTestPatternMax_R2 = 0x8000(Z);
		CC = LEDTestPattern_R1 < LEDTestPatternMax_R2;
		if !CC JUMP Else_LEDTestPatternTooLarge;
		//	{
			LEDTestPattern_R1 = LEDTestPattern_R1 << 1;	//	LEDTestPattern = LEDTestPattern << 1;
			JUMP DoneIf_LEDTestPattern;
		//	}
		Else_LEDTestPatternTooLarge:	//	else
		//	{
			LEDTestPattern_R1 = 0x1000(Z);	//	LEDTestPattern = 0x1000;
		//	}
		DoneIf_LEDTestPattern:
		
		//	CoffeePot_X->controlRegister = (CoffeePot_X->controlRegister & 0x0FFF) | LEDTestPattern;
		tempControlRegister_R0 = W[ptrCoffeePotXControlRegister_P0](Z);
		tempMaskREMOVE_CURRENT_LED_VALUES_R2 = MaskREMOVE_CURRENT_LED_VALUES(Z);
		tempControlRegister_R0 = tempControlRegister_R0 & tempMaskREMOVE_CURRENT_LED_VALUES_R2;
		tempControlRegister_R0 = tempControlRegister_R0 | LEDTestPattern_R1;
		W[ptrCoffeePotXControlRegister_P0] = tempControlRegister_R0;
		
		//	FastForward_OneSimulationTIC(CoffeePot_X);
		//	Saving volitile registers
		[--SP] = LEDTestPattern_R1;
		[--SP] = ptrCoffeePotX_P0;

		PassParameterToFF_R0 = ptrCoffeePotX_P0; //Setup the parameters for _FastForward_OneSimulationTIC
		Call __Z28FastForward_OneSimulationTICP16COFFEEPOT_DEVICE;
		
		//Restoring volitile registers
		ptrCoffeePotX_P0 = [SP++];
		LEDTestPattern_R1 = [SP++];
				
		JUMP StartForeverLoop;
	EndForeverLoop:
	//}

//Code should never get here
UNLINK;
_InitLED_TestLED_ASM.END:
RTS;