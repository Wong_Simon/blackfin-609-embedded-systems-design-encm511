#include <sys/platform.h>
#include "Coffeepot_Assignment1n2_Sim533.h"

//#define INIT_FOUR_COFFEEPOTS
#ifdef INIT_FOUR_COFFEEPOTS
	#define INIT_NUMBER_OF_COFFEEPOTS 4
#else
	#define INIT_NUMBER_OF_COFFEEPOTS 1
#endif



void My_CoffeePot_Main(){

	My_Init_CoffeePots(INIT_NUMBER_OF_COFFEEPOTS);
	COFFEEPOT_DEVICE * ActiveCoffeePot_Array [INIT_NUMBER_OF_COFFEEPOTS];

	//Overall_CoffeePot_Control_Test(COFFEEPOT1, "SSW");

	ActiveCoffeePot_Array[0] = My_Plug_TurnOn_InitAll_AddCoffee_CoffeePot(COFFEEPOT1, "SSW");

#ifdef INIT_FOUR_COFFEEPOTS
	ActiveCoffeePot_Array[1] = My_Plug_TurnOn_InitAll_AddCoffee_CoffeePot(COFFEEPOT2, "SSW");
	ActiveCoffeePot_Array[2] = My_Plug_TurnOn_InitAll_AddCoffee_CoffeePot(COFFEEPOT3, "SSW");
	ActiveCoffeePot_Array[3] = My_Plug_TurnOn_InitAll_AddCoffee_CoffeePot(COFFEEPOT4, "SSW");
#endif

	Test_Heater_Failure(ActiveCoffeePot_Array[0]);
	//Test_Water_Failure(ActiveCoffeePot_Array[0]);
	//Test_LED(ActiveCoffeePot_Array[0]);
	//InitLED_TestLED_ASM(ActiveCoffeePot_Array[0]);

	unsigned long int TIC_Count = 0;

	while(1){
		for (short int CoffeePot_ID_Index = 0; CoffeePot_ID_Index < INIT_NUMBER_OF_COFFEEPOTS; CoffeePot_ID_Index++){

			COFFEEPOT_DEVICE * CurrentCoffeePot = ActiveCoffeePot_Array[CoffeePot_ID_Index];
			My_Heater_Control(CurrentCoffeePot, false);
			My_Water_Control(CurrentCoffeePot, false);
			FastForward_OneSimulationTIC(CurrentCoffeePot);
			if (TIC_Count % 10 == 0){
				printf("Pot: %1i;  TIC Count: %5d;  ;  ControlReg: 0x%04x;  WaterLevel: %5d;  HeaterReg: %3d;  HeaterBoostReg: %2d;  Temperature: %4d\n", CoffeePot_ID_Index+1, TIC_Count, CurrentCoffeePot->controlRegister, CurrentWaterLevel_CPP(CurrentCoffeePot), CurrentCoffeePot->heaterRegister, CurrentCoffeePot->heaterBoostRegister, CurrentTemperature_CPP(CurrentCoffeePot));
			}
		}
		TIC_Count++;
	}

	//printf("DEBUG Current Control Register values: 0b%b16\n", CoffeePot_1->controlRegister);
}


void My_Init_CoffeePots(unsigned short int NumberOfCoffeePots){
	Show_Debug_Console_Information("DEBUG: Stub My_Init_CoffeePot\n");
	Init_CoffeePotSimulation(NumberOfCoffeePots, USE_TEXT_AND_GRAPHICS_GUIS);
}

COFFEEPOT_DEVICE * My_Plug_TurnOn_CoffeePot(COFFEEPOT_ID COFFEEPOT, char coffeePotName[]){
	Show_Debug_Console_Information("DEBUG: Stub My_Plug_TurnOn_CoffeePot\n");

	//plug coffeepot in
	COFFEEPOT_DEVICE * CoffeePot_X = Add_CoffeePotToSystem_PlugAndPlay(COFFEEPOT, coffeePotName);

	//turn on
	CoffeePot_X->controlRegister = CoffeePot_X->controlRegister | INITandSTAYPOWEREDON_BIT;

	//poll the coffee pot for ready state, wait until ready
	while ((CoffeePot_X->controlRegister & DEVICE_READY_BIT_RO) != DEVICE_READY_BIT_RO){
		FastForward_OneSimulationTIC(CoffeePot_X);
		Show_Debug_Console_Information("DEBUG: Waiting for coffee pot to poll\n");
	}

	Show_Debug_Console_Information("DEBUG: Stub My_Plug_TurnOn_CoffeePot: Success\n");
	return CoffeePot_X;
}

void My_Init_LED_CoffeePot(COFFEEPOT_DEVICE * CoffeePot_X){
	Show_Debug_Console_Information("DEBUG: Stub My_Init_LED_CoffeePot\n");
	CoffeePot_X->controlRegister = CoffeePot_X->controlRegister | LED_DISPLAY_ENABLE_BIT;
	CoffeePot_X->controlRegister = CoffeePot_X->controlRegister | USE_LED4_TO_SHOW_LED_DISPLAY_ENABLED;
	CoffeePot_X->controlRegister = CoffeePot_X->controlRegister | USE_LED1_TO_SHOW_SYSTEM_POWEREDUP; //This command would only run if the coffeepot was ready to play
}

void Test_LED(COFFEEPOT_DEVICE * CoffeePot_X){
	Show_Debug_Console_Information("DEBUG: Stub Test_LED\n");
	unsigned short int LEDTestPattern = 0x1000;
	while(1){
		if (LEDTestPattern < 0x8000){
			LEDTestPattern = LEDTestPattern << 1;
		}
		else {
			LEDTestPattern = 0x1000;
		}
		CoffeePot_X->controlRegister = (CoffeePot_X->controlRegister & 0x0FFF) | LEDTestPattern; //clear current LED pattern, then write new LED pattern
		FastForward_OneSimulationTIC(CoffeePot_X);
	}
}

void My_Init_Water_CoffeePot(COFFEEPOT_DEVICE * CoffeePot_X){
	Show_Debug_Console_Information("DEBUG: Stub My_Init_Water_CoffeePot\n");
	CoffeePot_X->controlRegister = CoffeePot_X->controlRegister | WATER_ENABLE_BIT;
	CoffeePot_X->controlRegister = CoffeePot_X->controlRegister | USE_LED3_TO_SHOW_WATER_ENABLED;
}

void My_Init_Heater_CoffeePot(COFFEEPOT_DEVICE * CoffeePot_X){
	Show_Debug_Console_Information("DEBUG: Stub My_Init_Heater_CoffeePot\n");
	CoffeePot_X->controlRegister = CoffeePot_X->controlRegister | HEATER_ENABLE_BIT;
	CoffeePot_X->heaterBoostRegister = HEATBOOSTREG;
	CoffeePot_X->controlRegister = CoffeePot_X->controlRegister | USE_LED2_TO_SHOW_HEATER_ENABLED;
}

void Test_Water_Failure(COFFEEPOT_DEVICE * CoffeePot_X){
	Show_Debug_Console_Information("DEBUG: Stub Test_Water_Failure\n");
	//Test to failure water
	while(1){
	CoffeePot_X->waterInFlowRegister = WATER_INFLOW_MAX;
	//printf("DEBUG: Current waterInFlowRegister: %d\n", CoffeePot_X->waterInFlowRegister);
	//printf("DEBUG: Current CurrentWaterLevel: %d\n", CurrentWaterLevel_CPP(CoffeePot_X));
	FastForward_OneSimulationTIC(CoffeePot_X);
	}
}

void Test_Heater_Failure(COFFEEPOT_DEVICE * CoffeePot_X){
	Show_Debug_Console_Information("DEBUG: Stub Test_Heater_Failure\n");
	//Test to failure heating
	while(1){
		CoffeePot_X->heaterRegister = HEATER_REG_MAX;
		//printf("DEBUG: Current heaterRegister: %d\n", CoffeePot_X->heaterRegister);
		//printf("DEBUG: Current CurrentWaterLevel: %d\n", CurrentTemperature_CPP(CoffeePot_X));
		FastForward_OneSimulationTIC(CoffeePot_X);
	}
}

void Fill_CoffeePot_MaxWater(COFFEEPOT_DEVICE * CoffeePot_X){
	Show_Debug_Console_Information("DEBUG: Stub Fill_CoffeePot_MaxWater\n");
	CoffeePot_X->waterInFlowRegister = WATER_INFLOW_MAX;
}

void My_Water_Control(COFFEEPOT_DEVICE * CoffeePot_X, bool CoffeeIsReady){
	Show_Debug_Console_Information("DEBUG: Stub My_Water_Control\n");
	if (!CoffeeIsReady){
		unsigned int CurrentTotalWater = CurrentWaterLevel_CPP(CoffeePot_X);
		unsigned int CurrentTemperature = CurrentTemperature_CPP(CoffeePot_X);

		//Since I'm in this function, lets turn the LED on unless later told to be off
		CoffeePot_X->controlRegister = CoffeePot_X->controlRegister | USE_LED3_TO_SHOW_WATER_ENABLED;

		if (CoffeePot_X->waterInFlowRegister > EVAPORATION_RATE+EVAPORATION_OFFSET) {
			//When filling pot initially, do nothing
		}
		else if ((CurrentTemperature >= BOILING_TEMP) && (CurrentTotalWater <= .9 * MAX_WATER_LEVEL)){
			CoffeePot_X->waterInFlowRegister = CoffeePot_X->waterInFlowRegister > EVAPORATION_RATE+EVAPORATION_OFFSET-HALF_WATER_REG_DECAY ? CoffeePot_X->waterInFlowRegister : EVAPORATION_RATE+EVAPORATION_OFFSET+HALF_WATER_REG_DECAY;
		}
		else if (CurrentTotalWater <= .9 * MAX_WATER_LEVEL){
			//Top-off after temp stabilizes to >90% water fill
			CoffeePot_X->waterInFlowRegister = CoffeePot_X->waterInFlowRegister > EVAPORATION_RATE-HALF_WATER_REG_DECAY ? CoffeePot_X->waterInFlowRegister : EVAPORATION_RATE-HALF_WATER_REG_DECAY;
		}
		else
		{
			//Turn Off Water LED
			CoffeePot_X->controlRegister = CoffeePot_X->controlRegister & (~USE_LED3_TO_SHOW_WATER_ENABLED);
		}
	}
}

void My_Heater_Control(COFFEEPOT_DEVICE * CoffeePot_X, bool CoffeeIsReady){
	Show_Debug_Console_Information("DEBUG: Stub My_Heater_Control\n");
	if (!CoffeeIsReady){
		unsigned int CurrentTemperature = CurrentTemperature_CPP(CoffeePot_X);

		//Since I'm in this function, lets turn the LED on unless later told to be off
		CoffeePot_X->controlRegister = CoffeePot_X->controlRegister | USE_LED2_TO_SHOW_HEATER_ENABLED;

		if (CurrentTemperature >= IDEAL_TEMP * 0.9){
			// Set heat to idle (after heat decays away)
			//Turn off heat LED
			CoffeePot_X->controlRegister = CoffeePot_X->controlRegister & (~USE_LED2_TO_SHOW_HEATER_ENABLED);
		}
		else {// if (CurrentTemperature < BOILING_TEMP * 0.9){
			CoffeePot_X->heaterRegister = HEATER_REG_MAX;
		}
		//else{
		//	CoffeePot_X->heaterRegister = CoffeePot_X->heaterRegister + HEATER_REG_DECAY;
		//}
	}
}

void Add_Coffee(COFFEEPOT_DEVICE * CoffeePot_X){
	Show_Debug_Console_Information("DEBUG: Stub Add_Coffee\n");
	CoffeePot_X->controlRegister = CoffeePot_X->controlRegister | COFFEEPOT_INSERTED_FIXED;
}

COFFEEPOT_DEVICE * My_Plug_TurnOn_InitAll_AddCoffee_CoffeePot(COFFEEPOT_ID COFFEEPOT, char coffeePotName[]){
	Show_Debug_Console_Information("DEBUG: Stub My_Plug_TurnOn_InitAll_AddCoffee_CoffeePot\n");
	COFFEEPOT_DEVICE * CoffeePot_X = My_Plug_TurnOn_CoffeePot(COFFEEPOT, coffeePotName);
	My_Init_LED_CoffeePot(CoffeePot_X);
	My_Init_Water_CoffeePot(CoffeePot_X);
	My_Init_Heater_CoffeePot(CoffeePot_X);
	Fill_CoffeePot_MaxWater(CoffeePot_X);
	Add_Coffee(CoffeePot_X);
	return CoffeePot_X;
}

void Overall_CoffeePot_Control_Test(COFFEEPOT_ID COFFEEPOT, char coffeePotName[]){
	Show_Debug_Console_Information("Start DEBUG: Stub Overall_CoffeePot_Control_Test\n");

	//////////////////////////////////////////////////////////////////////////////////////

	COFFEEPOT_DEVICE * CoffeePot_X = My_Plug_TurnOn_CoffeePot(COFFEEPOT, coffeePotName);
	if ((CoffeePot_X->controlRegister & INITandSTAYPOWEREDON_BIT) == INITandSTAYPOWEREDON_BIT){
		printf("SUCCESS TEST INITandSTAYPOWEREDON_BIT: CoffeePot was turned on.\n");
	}
	else
	{
		printf("FAILURE TEST INITandSTAYPOWEREDON_BIT: CoffeePot did not turn on.\n");
	}
	if ((CoffeePot_X->controlRegister & DEVICE_READY_BIT_RO) == DEVICE_READY_BIT_RO){
		printf("SUCCESS TEST DEVICE_READY_BIT_RO: CoffeePot waited until it was ready.\n");
	}
	else
	{
		printf("FAILURE TEST DEVICE_READY_BIT_RO: CoffeePot did not wait until ready.\n");
	}

	//////////////////////////////////////////////////////////////////////////////////////

	My_Init_LED_CoffeePot(CoffeePot_X);
	if ((CoffeePot_X->controlRegister & LED_DISPLAY_ENABLE_BIT) == LED_DISPLAY_ENABLE_BIT){
		printf("SUCCESS TEST LED_DISPLAY_ENABLE_BIT: LED display was activated.\n");
	}
	else
	{
		printf("FAILURE TEST LED_DISPLAY_ENABLE_BIT: LED display was not activated.\n");
	}

	//////////////////////////////////////////////////////////////////////////////////////

	CoffeePot_X->controlRegister = CoffeePot_X->controlRegister | 0xF000; //Turn on all the LEDs
	if ((CoffeePot_X->controlRegister & 0xF000) == 0xF000){
		printf("SUCCESS TEST LED BITS: LEDs can be used.\n");
	}
	else
	{
		printf("FAILURE TEST LED BITS: LEDs can not be used.\n");
	}

	//////////////////////////////////////////////////////////////////////////////////////

	My_Init_Water_CoffeePot(CoffeePot_X);
	if ((CoffeePot_X->controlRegister & WATER_ENABLE_BIT) == WATER_ENABLE_BIT){
		printf("SUCCESS TEST WATER_ENABLE_BIT: Water was activated.\n");
	}
	else
	{
		printf("FAILURE TEST WATER_ENABLE_BIT: Water was not activated.\n");
	}

	//////////////////////////////////////////////////////////////////////////////////////

	My_Init_Heater_CoffeePot(CoffeePot_X);
	if ((CoffeePot_X->controlRegister & HEATER_ENABLE_BIT) == HEATER_ENABLE_BIT){
		printf("SUCCESS TEST HEATER_ENABLE_BIT: Heater was activated.\n");
	}
	else
	{
		printf("FAILURE TEST HEATER_ENABLE_BIT: Heater was not activated.\n");
	}
	if ((CoffeePot_X->heaterBoostRegister) != 0){
		printf("SUCCESS TEST heaterBoostRegister: heaterBoostRegister was assigned a non-zero value.\n");
	}
	else
	{
		printf("FAILURE TEST heaterBoostRegister: heaterBoostRegister was not assigned a value.\n");
	}
	//////////////////////////////////////////////////////////////////////////////////////

	Fill_CoffeePot_MaxWater(CoffeePot_X);
	if ((CoffeePot_X->waterInFlowRegister) == WATER_INFLOW_MAX){
		printf("SUCCESS TEST Fill_CoffeePot_MaxWater: waterInFlowRegister was assigned the value WATER_INFLOW_MAX.\n");
	}
	else
	{
		printf("FAILURE TEST Fill_CoffeePot_MaxWater: waterInFlowRegister was not assigned the value WATER_INFLOW_MAX.\n");
	}

	//////////////////////////////////////////////////////////////////////////////////////

	Add_Coffee(CoffeePot_X);
	if ((CoffeePot_X->controlRegister & COFFEEPOT_INSERTED_FIXED) == COFFEEPOT_INSERTED_FIXED){
		printf("SUCCESS TEST COFFEEPOT_INSERTED_FIXED: Coffee bag was added.\n");
	}
	else
	{
		printf("FAILURE TEST COFFEEPOT_INSERTED_FIXED: Coffee bag was added.\n");
	}

	//////////////////////////////////////////////////////////////////////////////////////

	CoffeePot_X->heaterRegister = HEATER_REG_MAX;
	if (CoffeePot_X->heaterRegister  == HEATER_REG_MAX){
		printf("SUCCESS TEST heaterRegister: heaterRegister was assigned HEATER_REG_MAX.\n");
	}
	else
	{
		printf("FAILURE TEST heaterRegister: heaterRegister was not assigned a value.\n");
	}


	Show_Debug_Console_Information("Completed DEBUG: Stub Overall_CoffeePot_Control_Test\n");
	while(1){} //Done testing - IDLE
}
