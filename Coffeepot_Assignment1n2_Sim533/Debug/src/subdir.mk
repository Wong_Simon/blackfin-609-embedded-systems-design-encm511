################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
ASM_SRCS += \
../src/CoffeePotASM.asm 

CPP_SRCS += \
../src/Coffeepot_Assignment1n2_Sim533.cpp \
../src/GeneralCode.cpp 

SRC_OBJS += \
./src/CoffeePotASM.doj \
./src/Coffeepot_Assignment1n2_Sim533.doj \
./src/GeneralCode.doj 

ASM_DEPS += \
./src/CoffeePotASM.d 

CPP_DEPS += \
./src/Coffeepot_Assignment1n2_Sim533.d \
./src/GeneralCode.d 


# Each subdirectory must supply rules for building sources it contributes
src/CoffeePotASM.doj: ../src/CoffeePotASM.asm
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin Assembler'
	easmblkfn -file-attr ProjectName="Coffeepot_Assignment1n2_Sim533" -proc ADSP-BF533 -si-revision any -g -DCORE0 -D_DEBUG @includes-639992f5a5d936b1ef2dee6b4eaf684a.txt -gnu-style-dependencies -MM -Mo "src/CoffeePotASM.d" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/Coffeepot_Assignment1n2_Sim533.doj: ../src/Coffeepot_Assignment1n2_Sim533.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn -c -file-attr ProjectName="Coffeepot_Assignment1n2_Sim533" -proc ADSP-BF533 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG @includes-da04315b6ce8127ccb5e5fb8c0abaa98.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/Coffeepot_Assignment1n2_Sim533.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/GeneralCode.doj: ../src/GeneralCode.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: CrossCore Blackfin C/C++ Compiler'
	ccblkfn -c -file-attr ProjectName="Coffeepot_Assignment1n2_Sim533" -proc ADSP-BF533 -flags-compiler --no_wrap_diagnostics -si-revision any -g -DCORE0 -D_DEBUG @includes-da04315b6ce8127ccb5e5fb8c0abaa98.txt -structs-do-not-overlap -no-const-strings -no-multiline -warn-protos -double-size-32 -decls-strong -cplbs -gnu-style-dependencies -MD -Mo "src/GeneralCode.d" -c++ -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


